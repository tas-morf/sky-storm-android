package com.accedo.android.skystorm.response;

import com.accedo.android.skystorm.model.bean.server.ServerUserData;

public class ServerUserDataResponses {
    public static final String A_SESSION_ID = "aSessionId";
    public static final int REMAINING_ATTEMPTS = 1;
    public static final String A_CUSTOMER_ID = "acustomerId";
    public static final String A_FIRST_NAME = "afirstname";
    public static final String A_LAST_NAME = "alastname";

    private static ServerUserData.Builder koResponseBuilder() {
        return ServerUserData.newBuilder()
                .resultMessage("KO")
                .skygoSessionId(A_SESSION_ID)
                .remainingAttempt(REMAINING_ATTEMPTS);
    }

    public static ServerUserData noTnCResponse() {
        return koResponseBuilder().resultCode("T_215").build();
    }

    public static ServerUserData sessionBlockedResponse() {
        return koResponseBuilder().resultCode("T_200").build();
    }

    public static ServerUserData wrongPinResponse() {
        return koResponseBuilder().resultCode("T_201").build();
    }

    public static ServerUserData accountLockedResponse() {
        return koResponseBuilder().resultCode("T_202").build();
    }

    public static ServerUserData alreadyLoggedInResponse() {
        return koResponseBuilder().resultCode("T_206").build();
    }

    public static ServerUserData customerNotFoundResponse() {
        return koResponseBuilder().resultCode("T_207").build();
    }

    public static ServerUserData tibcoErrorResponse() {
        return koResponseBuilder().resultCode("T_242").build();
    }

    public static ServerUserData normalResponse() {
        return ServerUserData.newBuilder()
                .resultMessage("OK")
                .skygoSessionId(A_SESSION_ID)
                .customerId(A_CUSTOMER_ID)
                .firstName(A_FIRST_NAME)
                .lastName(A_LAST_NAME)
                .build();
    }
}
