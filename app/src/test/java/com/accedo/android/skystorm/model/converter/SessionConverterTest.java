package com.accedo.android.skystorm.model.converter;

import com.accedo.android.skystorm.model.bean.client.Session;
import com.accedo.android.skystorm.model.bean.client.SessionError;
import com.accedo.android.skystorm.response.ServerUserDataResponses;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

public class SessionConverterTest {

    private SessionConverter converter = new SessionConverter();

    @Test
    public void noTncResponseIsCorrectError() {
        Session session = converter.convert(ServerUserDataResponses.noTnCResponse());
        assertTrue(session.isError());
        assertThat(session.getSessionError(), equalTo(SessionError.NO_TNC_ACCEPTED));
        //make sure the session id is there, because we are going to need it to accept the terms
        assertThat(session.getSessionId(), equalTo(ServerUserDataResponses.A_SESSION_ID));
    }

    @Test
    public void sessionBlockedResponseIsCorrectError() {
        Session session = converter.convert(ServerUserDataResponses.sessionBlockedResponse());
        assertTrue(session.isError());
        assertThat(session.getSessionError(), equalTo(SessionError.SESSION_BLOCKED));
    }

    @Test
    public void wrongPinResponseIsCorrectError() {
        Session session = converter.convert(ServerUserDataResponses.wrongPinResponse());
        assertTrue(session.isError());
        assertThat(session.getSessionError(), equalTo(SessionError.WRONG_PIN));
        assertThat(session.getRemainingAttempts(), equalTo(ServerUserDataResponses.REMAINING_ATTEMPTS));
    }

    @Test
    public void accountLockedResponseIsCorrectError() {
        Session session = converter.convert(ServerUserDataResponses.accountLockedResponse());
        assertTrue(session.isError());
        assertThat(session.getSessionError(), equalTo(SessionError.ACCOUNT_LOCKED));
    }

    @Test
    public void alreadyLoggedInResponseIsCorrectError() {
        Session session = converter.convert(ServerUserDataResponses.alreadyLoggedInResponse());
        assertTrue(session.isError());
        assertThat(session.getSessionError(), equalTo(SessionError.ALREADY_LOGGED_IN));
        //make sure the session id is there, because we are going to need it to kill the service
        assertThat(session.getSessionId(), equalTo(ServerUserDataResponses.A_SESSION_ID));
    }

    @Test
    public void customerNotFoundResponseIsCorrectError() {
        Session session = converter.convert(ServerUserDataResponses.customerNotFoundResponse());
        assertTrue(session.isError());
        assertThat(session.getSessionError(), equalTo(SessionError.CUSTOMER_NOT_FOUND));
    }

    @Test
    public void tibcoErrorResponseIsCorrectError() {
        Session session = converter.convert(ServerUserDataResponses.tibcoErrorResponse());
        assertTrue(session.isError());
        assertThat(session.getSessionError(), equalTo(SessionError.SERVER_ERROR));
    }

    @Test
    public void normalSessionGetsConvertedCorrectly() {
        Session session = converter.convert(ServerUserDataResponses.normalResponse());
        assertFalse(session.isError());
        assertThat(session.getSessionId(), equalTo(ServerUserDataResponses.A_SESSION_ID));
        assertThat(session.getCustomerId(), equalTo(ServerUserDataResponses.A_CUSTOMER_ID));
        assertThat(session.getName(), equalTo(ServerUserDataResponses.A_FIRST_NAME + " " +  ServerUserDataResponses
                .A_LAST_NAME));

    }
}