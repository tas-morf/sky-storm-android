package com.accedo.android.skystorm.response;

import com.accedo.android.skystorm.model.bean.server.ServerChannel;

/**
 * Contains ServerChannel items for testing
 */
public class ServerChannels {
    public static final String SKY_ATLANTIC_TITLE = "Sky Atlantic HD";
    public static final int SKY_ATLANTIC_ID = 2;
    public static final String SKY_ATLANTIC_MEDIA_URL = "aMediaUrl";
    public static final String SKY_ATLANTIC_IMAGE_URL = "/bin/EPGChannel/web/channel_36.png";
    public static final String SKY_ATLANTIC_COLOR = "acolor";

    public static ServerChannel skyAtlantic() {
        return ServerChannel
                .newBuilder()
                .color(SKY_ATLANTIC_COLOR)
                .mediaUrl(SKY_ATLANTIC_MEDIA_URL)
                .hd(0)
                .id(SKY_ATLANTIC_ID)
                .logo(SKY_ATLANTIC_IMAGE_URL)
                .mobilepc("")
                .name(SKY_ATLANTIC_TITLE)
                .num(0)
                .ppv(0)
                .build();
    }
}
