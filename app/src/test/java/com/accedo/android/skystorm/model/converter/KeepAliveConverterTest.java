package com.accedo.android.skystorm.model.converter;

import com.accedo.android.skystorm.model.bean.client.KeepAlive;
import com.accedo.android.skystorm.model.bean.server.ServerKeepAliveResponse;
import com.accedo.android.skystorm.response.ServerKeepAliveResponses;

import org.junit.Test;

import static org.junit.Assert.*;

public class KeepAliveConverterTest {

    KeepAliveConverter converter = new KeepAliveConverter();

    @Test
    public void koResponseIsNotSuccess() {
        KeepAlive keepAlive = converter.convert(ServerKeepAliveResponses.koResponse());
        assertFalse(keepAlive.isSuccess());
    }

    @Test
    public void okResponseIsSuccess() {
        KeepAlive keepAlive = converter.convert(ServerKeepAliveResponses.okResponseNoReloadData());
        assertTrue(keepAlive.isSuccess());
        assertFalse(keepAlive.isReloadUserData());
    }

    @Test
    public void okResponseWithReloadDataFlagIsHandledCorrectly() {
        KeepAlive keepAlive = converter.convert(ServerKeepAliveResponses.okResponseWithReloadData());
        assertTrue(keepAlive.isSuccess());
        assertTrue(keepAlive.isReloadUserData());
    }
}