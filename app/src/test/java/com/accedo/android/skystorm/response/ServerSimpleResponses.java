package com.accedo.android.skystorm.response;

import com.accedo.android.skystorm.model.bean.server.ServerSimpleResponse;

public class ServerSimpleResponses {
    public static ServerSimpleResponse okResponse() {
        return ServerSimpleResponse.newBuilder().resultCode("T_100").resultMessage("OK").build();
    }

    public static ServerSimpleResponse koResponse() {
        return ServerSimpleResponse.newBuilder().resultCode("T_200").resultMessage("KO").build();
    }
}
