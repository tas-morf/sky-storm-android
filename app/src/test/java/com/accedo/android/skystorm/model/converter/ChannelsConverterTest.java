package com.accedo.android.skystorm.model.converter;

import android.content.res.Resources;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.model.bean.client.Channel;
import com.accedo.android.skystorm.model.bean.server.ServerChannelsResponse;
import com.accedo.android.skystorm.response.ServerChannels;
import com.accedo.android.skystorm.response.ServerChannelsResponses;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(RobolectricTestRunner.class)
public class ChannelsConverterTest {

    private ServerChannelsResponse serverResponse = ServerChannelsResponses.singleSkyAtlanticResponse();
    private List<Channel> result;

    private final Resources resources = Robolectric.application.getResources();
    private ChannelsConverter converter = new ChannelsConverter(resources);

    @Before
    public void setup() {
        result = converter.convert(serverResponse);
    }

    @Test
    public void hasSameSize() {
        assertThat(result.size(), equalTo(serverResponse.getChannelList().size()));
    }

    @Test
    public void usesSameTitle() {
        assertThat(result.get(0).getTitle(), equalTo(ServerChannels.SKY_ATLANTIC_TITLE));
    }

    @Test
    public void usesSameId() {
        assertThat(result.get(0).getId(), equalTo(String.valueOf(ServerChannels.SKY_ATLANTIC_ID)));
    }

    @Test
    public void usesSameMediaUrl() {
        assertThat(result.get(0).getMediaUrl(), equalTo(ServerChannels.SKY_ATLANTIC_MEDIA_URL));
    }

    @Test
    public void usesThumbnailImage() {
        assertThat(result.get(0).getImageUrl(), equalTo(resources.getString(R.string.base_url) + ServerChannels
                .SKY_ATLANTIC_IMAGE_URL));
    }
}
