package com.accedo.android.skystorm.model.converter;

import android.content.res.Resources;

import com.accedo.android.skystorm.model.bean.client.Channel;
import com.accedo.android.skystorm.model.bean.server.ServerChannelsResponse;
import com.accedo.android.skystorm.model.bean.server.ServerSimpleResponse;
import com.accedo.android.skystorm.response.ServerChannelsResponses;
import com.accedo.android.skystorm.response.ServerSimpleResponses;

import org.junit.Before;
import org.junit.Test;
import org.robolectric.Robolectric;

import java.util.List;

import static org.junit.Assert.*;

public class LogoutConverterTest {


    private LogoutConverter converter = new LogoutConverter();

    @Test
    public void okResponseReturnsTrue() {
        assertTrue(converter.convert(ServerSimpleResponses.okResponse()));
    }

    @Test
    public void koResponseReturnsFalse() {
        assertFalse(converter.convert(ServerSimpleResponses.koResponse()));
    }
}