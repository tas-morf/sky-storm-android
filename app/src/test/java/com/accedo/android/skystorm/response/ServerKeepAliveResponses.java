package com.accedo.android.skystorm.response;

import com.accedo.android.skystorm.model.bean.server.ServerKeepAliveResponse;

public class ServerKeepAliveResponses {
    public static ServerKeepAliveResponse koResponse() {
        return ServerKeepAliveResponse.newBuilder().resultMessage("KO").build();
    }

    public static ServerKeepAliveResponse okResponseNoReloadData() {
        return ServerKeepAliveResponse.newBuilder().resultMessage("OK").build();
    }

    public static ServerKeepAliveResponse okResponseWithReloadData() {
        return ServerKeepAliveResponse.newBuilder()
                .resultMessage("OK")
                .reloadUserData(true)
                .build();
    }
}
