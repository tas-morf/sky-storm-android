package com.accedo.android.skystorm.controller.fragment;

import android.app.Activity;
import android.view.View;

import com.accedo.android.skystorm.model.bean.client.Channel;
import com.accedo.android.skystorm.view.indicator.ChannelListIndicator;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.accedo.android.skystorm.fake.FakeLayoutInflater;
import com.accedo.android.skystorm.fake.FakeRequestFactory;
import com.accedo.android.skystorm.model.UrlProvider;
import com.accedo.android.skystorm.model.request.RequestFactory;
import com.accedo.android.skystorm.stub.StubRequest;
import com.accedo.android.skystorm.stub.StubUrlProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.util.ActivityController;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
public class ChannelListFragmentTest {

    //We need a context
    private Activity act = ActivityController.of(Activity.class).attach().get();

    //Stub
    private View stubView = new View(act);
    private VolleyError stubError = new VolleyError();
    private Request<List<Channel>> stubChannelListRequest = new StubRequest<List<Channel>>();
    private List<Channel> stubResponse = new ArrayList<Channel>();
    private UrlProvider stubUrlProvider = new StubUrlProvider();

    //Fake
    private RequestFactory<List<Channel>> fakeRequestFactory = new FakeRequestFactory<List<Channel>>(stubChannelListRequest);
    private FakeLayoutInflater fakeInflater = new FakeLayoutInflater(stubView);

    //Mocks
    @Mock
    private RequestQueue mockRequestQueue;
    @Mock
    private ChannelListIndicator mockIndicator;

    private ChannelListFragment sut;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        sut = new ChannelListFragment(mockRequestQueue, fakeRequestFactory, mockIndicator, stubUrlProvider);
        sut.onAttach(act);
    }

    @Test
    public void onCreateViewInitializesIndicator() {
        //when
        sut.onCreateView(fakeInflater, null, null);
        //then
        verify(mockIndicator).initialize(stubView, sut);
    }

    @Test
    public void onCreateViewDoesRequest() {
        //when
        sut.onCreateView(fakeInflater, null, null);
        //then
        verify(mockRequestQueue).add(stubChannelListRequest);
    }

    @Test
    public void onErrorResponseUsesIndicator() {
        //when
        sut.onErrorResponse(stubError);
        //then
        verify(mockIndicator).showError();
    }

    @Test
    public void onResponseUsesIndicator() {
        //when
        sut.onResponse(stubResponse);
        //then
        verify(mockIndicator).showData(stubResponse);
    }

}