package com.accedo.android.skystorm.response;

import com.accedo.android.skystorm.model.bean.server.ServerChannelsResponse;

import java.util.Arrays;

public class ServerChannelsResponses {

    public static ServerChannelsResponse singleSkyAtlanticResponse() {
        return ServerChannelsResponse.newBuilder().channelList(Arrays.asList(ServerChannels.skyAtlantic())).build();
    }
}
