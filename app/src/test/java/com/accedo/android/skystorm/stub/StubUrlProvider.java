package com.accedo.android.skystorm.stub;

import com.accedo.android.skystorm.model.UrlProvider;
import com.accedo.android.skystorm.model.bean.client.LoginCredentials;

public class StubUrlProvider implements UrlProvider {

    @Override
    public String allChannelsUrl() {
        return null;
    }

    @Override
    public String loginUrl(LoginCredentials loginCredentials) {
        return null;
    }

    @Override
    public String sessionUrl() {
        return null;
    }

    @Override
    public String userDataUrl() {
        return null;
    }

    @Override
    public String sessionKillUrl(String sessionId) {
        return null;
    }

    @Override
    public String getAcceptTermsUrl(String sessionId) {
        return null;
    }

    @Override
    public String navigationUrl() {
        return null;
    }

    @Override
    public String searchUrl(String searchTerm) {
        return null;
    }
}
