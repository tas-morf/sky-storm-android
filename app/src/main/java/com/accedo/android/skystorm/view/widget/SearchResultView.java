package com.accedo.android.skystorm.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.model.bean.client.SearchResult;
import com.android.volley.toolbox.ImageLoader;

import static com.accedo.android.skystorm.module.view.ImageLoaderModule.imageLoader;

/**
 * Represents a single search result
 */
public class SearchResultView extends LinearLayout {
    private ImageView image;
    private TextView title;
    private TextView info;
    private ImageLoader imageLoader;

    public SearchResultView(Context context) {
        super(context);
        postConstruct(imageLoader());
    }

    public SearchResultView(Context context, AttributeSet attrs) {
        super(context, attrs);
        postConstruct(imageLoader());
    }

    public SearchResultView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        postConstruct(imageLoader());
    }

    private void postConstruct(ImageLoader imageLoader) {
        this.imageLoader = imageLoader;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        image = (ImageView)findViewById(R.id.result_image);
        title = (TextView)findViewById(R.id.result_title);
        info = (TextView)findViewById(R.id.result_info);
    }

    public void setData(SearchResult searchResult) {
        title.setText(searchResult.getTitle());
        info.setText("|" + searchResult.getCategory() + "|" + searchResult.getParentalRating());
        if(searchResult.isHD()) {
            info.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        } else {
            info.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hd_icon, 0, 0, 0);
        }
        ImageLoader.ImageContainer imageContainer = (ImageLoader.ImageContainer) image.getTag();
        if(imageContainer == null || !imageContainer.getRequestUrl().equals(searchResult.getImageUrl())) {
            image.setTag(imageLoader.get(searchResult.getImageUrl(), ImageLoader.getImageListener(image,
                    R.drawable.ic_launcher, R.drawable.ic_launcher)));
        }
    }
}
