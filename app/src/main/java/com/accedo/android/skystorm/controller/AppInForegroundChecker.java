package com.accedo.android.skystorm.controller;

/**
 * Returns whether the application is in the foreground
 */
public interface AppInForegroundChecker {

    boolean isAppVisible();

    boolean isAppInForeground();
}
