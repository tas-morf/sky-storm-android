package com.accedo.android.skystorm.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.model.bean.client.NavigationItem;
import com.accedo.android.skystorm.view.widget.SideMenuRowView;

import java.util.List;

public class SideMenuAdapter extends BaseAdapter {

    private Context context;
    private List<NavigationItem> items;

    public SideMenuAdapter(Context context, List<NavigationItem> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.view_side_menu_row, viewGroup, false);
        }
        ((SideMenuRowView) convertView).setData(items.get(position));
        return convertView;
    }
}
