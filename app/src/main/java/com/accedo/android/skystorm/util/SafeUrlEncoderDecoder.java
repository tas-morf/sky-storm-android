package com.accedo.android.skystorm.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Does url encoding and decoding without throwing exceptions
 */
public class SafeUrlEncoderDecoder {

    public static String encodeUtf8(String string) {
        String result;
        try {
            result = URLEncoder.encode(string, "utf-8");
        } catch (UnsupportedEncodingException e) {
            return "";
        }
        return result;
    }
}
