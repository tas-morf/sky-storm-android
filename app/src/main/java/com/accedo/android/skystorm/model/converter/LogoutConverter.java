package com.accedo.android.skystorm.model.converter;

import com.accedo.android.skystorm.model.bean.server.ServerSimpleResponse;

public class LogoutConverter implements Converter<ServerSimpleResponse, Boolean> {
    @Override
    public Boolean convert(ServerSimpleResponse value) {
        return "OK".equals(value.getResultMessage());
    }
}
