package com.accedo.android.skystorm.view.indicator.impl;

import android.content.res.Resources;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.controller.listener.OnLoginEventListener;
import com.accedo.android.skystorm.model.bean.client.LoginCredentials;
import com.accedo.android.skystorm.model.bean.client.SessionError;
import com.accedo.android.skystorm.view.Toaster;
import com.accedo.android.skystorm.view.indicator.LoginIndicator;

import static com.accedo.android.skystorm.model.bean.client.LoginCredentials.aLoginCredentials;


public class LoginViewIndicator implements LoginIndicator, View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private final Toaster toaster;
    private Resources res;

    private TextView customerIdText;
    private TextView emailText;
    private TextView passwordText;
    private OnLoginEventListener listener;

    public LoginViewIndicator(Toaster toaster, Resources res) {
        this.toaster = toaster;
        this.res = res;
    }

    @Override
    public void initialize(View rootView, OnLoginEventListener listener) {
        this.listener = listener;
        ((RadioButton)rootView.findViewById(R.id.customer_id_radio)).setOnCheckedChangeListener(this);
        ((RadioButton)rootView.findViewById(R.id.email_radio)).setOnCheckedChangeListener(this);
        customerIdText = (TextView)rootView.findViewById(R.id.costumer_id_edit);
        emailText = (TextView)rootView.findViewById(R.id.email_edit);
        passwordText = (TextView)rootView.findViewById(R.id.password_edit);
        rootView.findViewById(R.id.login_button).setOnClickListener(this);
    }

    @Override
    public void showNetworkError() {
        toaster.showToast(R.string.network_error_toast);
    }

    @Override
    public void showGreeting(String name) {
        toaster.showToast(res.getString(R.string.greeting, name));
    }

    @Override
    public void showServerError(SessionError sessionError, int remainingAttempts) {
        if(sessionError == SessionError.WRONG_PIN) {
            toaster.showToast(res.getString(sessionError.getErrorStringId(), remainingAttempts));
        } else {
            toaster.showToast(sessionError.getErrorStringId());
        }
    }

    @Override
    public LoginCredentials getLoginCredentials() {
        return aLoginCredentials()
                .email(emailText.getText().toString())
                .customerId(customerIdText.getText().toString())
                .password(passwordText.getText().toString())
                .build();
    }

    @Override
    public void sessionKillFailed() {
        toaster.showToast(R.string.session_kill_error);
    }

    @Override
    public void onClick(View view) {
        listener.onLoginRequested(getLoginCredentials());
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if(b && compoundButton.getId() == R.id.customer_id_radio) {
            emailText.setText("");
            emailText.setEnabled(false);
            customerIdText.setEnabled(true);
            customerIdText.requestFocus();
        } else if(b && compoundButton.getId() == R.id.email_radio) {
            customerIdText.setText("");
            customerIdText.setEnabled(false);
            emailText.setEnabled(true);
            emailText.requestFocus();
        }
    }
}
