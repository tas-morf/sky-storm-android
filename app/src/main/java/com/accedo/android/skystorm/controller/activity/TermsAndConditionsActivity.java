package com.accedo.android.skystorm.controller.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.model.UrlProvider;
import com.accedo.android.skystorm.model.request.RequestFactory;
import com.accedo.android.skystorm.view.Toaster;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import static com.accedo.android.skystorm.module.model.RequestFactoryModule.simpleRequestFactory;
import static com.accedo.android.skystorm.module.model.RequestQueueModule.requestQueue;
import static com.accedo.android.skystorm.module.model.UrlProviderModule.urlProvider;
import static com.accedo.android.skystorm.module.view.ToasterModule.toaster;

/**
 * A dialog displaying the terms and conditions along with an OK button
 */
public class TermsAndConditionsActivity extends Activity implements View.OnClickListener, Response.Listener<Boolean>, Response.ErrorListener {

    private static final String PARAM_SESSION_ID = "sessionId";
    private RequestQueue requestQueue;
    private RequestFactory<Boolean> acceptTnCRequestFactory;
    private UrlProvider urlProvider;
    private Toaster toaster;

    private View progress;
    private View termsText;

    public TermsAndConditionsActivity() {
        this(requestQueue(), simpleRequestFactory(), urlProvider(), toaster());
    }

    public TermsAndConditionsActivity(RequestQueue requestQueue, RequestFactory<Boolean> acceptTnCRequestFactory, UrlProvider urlProvider, Toaster toaster) {
        this.requestQueue = requestQueue;
        this.acceptTnCRequestFactory = acceptTnCRequestFactory;
        this.urlProvider = urlProvider;
        this.toaster = toaster;
    }

    //since we haven't saved the session id yet, we need to pass it in
    public static Intent getTermsAndConditionsIntent(Context context, String sessionId) {
        Intent intent = new Intent(context, TermsAndConditionsActivity.class);
        intent.putExtra(PARAM_SESSION_ID, sessionId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);
        progress = findViewById(R.id.progress);
        termsText = findViewById(R.id.terms_text);

        findViewById(R.id.accept_button).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        requestQueue.add(acceptTnCRequestFactory.createRequest(Request.Method.PUT,
                urlProvider.getAcceptTermsUrl(getIntent().getStringExtra(PARAM_SESSION_ID)),
                this,
                this));
        progress.setVisibility(View.VISIBLE);
        termsText.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(Boolean response) {
        if (response) {
            setResult(RESULT_OK);
            finish();
        } else {
            onErrorResponse(null);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        toaster.showToast(R.string.accept_terms_failure);
    }
}
