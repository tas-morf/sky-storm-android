package com.accedo.android.skystorm.controller;

import android.app.Fragment;

import com.accedo.android.skystorm.controller.fragment.EmptyFragment;
import com.accedo.android.skystorm.model.bean.client.NavigationItem;
import com.accedo.android.skystorm.model.bean.client.NavigationItemType;

public class DefaultNavigationFragmentFactory implements NavigationFragmentFactory {

    @Override
    public Fragment fragmentFor(NavigationItem navItem) {
        //if it has sub items, simply return the fragment for the first sub item
        if (navItem.getType() == NavigationItemType.TOP_SECTION) {
            return fragmentFor(navItem.getSubNavigationItems().get(0));
        }
        //TODO: create a fragment for each type
        switch (navItem.getType()) {
            case HOME:
                break;
            case SKY_GUIDE:
                break;
            case TOP_SECTION:
                break;
            case SPORT_HIGHLIGHTS:
                break;
            case LIVE_PLANNER_SECTION:
                break;
            case LIVE_PLANNER_ITEM:
                break;
            case SPORTS_NEWS_SECTION:
                break;
            case SPORTS_NEWS_ITEM:
                break;
            case SPORTS_CLIPS_SECTION:
                break;
            case SPORTS_CLIPS_ITEM:
                break;
            case SPORT_REPLAYS_SECTION:
                break;
            case SPORT_REPLAYS_ITEM:
                break;
            case WEBVIEW:
                break;
            case HIGHLIGHTS:
                break;
            case WHOLE_CATALOG:
                break;
            case SUBCATALOG:
                break;
            case SINGLE_LISTING:
                break;
            case DOUBLE_LISTING:
                break;
            case LICENSORS:
                break;
        }
        return EmptyFragment.newInstance(navItem.getTitle());
    }
}
