package com.accedo.android.skystorm.controller.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.controller.activity.LoginActivity;
import com.accedo.android.skystorm.controller.listener.OnLogoutEventListener;
import com.accedo.android.skystorm.controller.listener.OnLogoutProcessFinishedListener;
import com.accedo.android.skystorm.controller.service.KeepAliveIntentService;
import com.accedo.android.skystorm.model.UrlProvider;
import com.accedo.android.skystorm.model.bean.client.KeepAliveCommand;
import com.accedo.android.skystorm.model.persistence.SimplePreferences;
import com.accedo.android.skystorm.model.request.RequestFactory;
import com.accedo.android.skystorm.view.indicator.LogoutIndicator;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import static com.accedo.android.skystorm.module.model.RequestFactoryModule.simpleRequestFactory;
import static com.accedo.android.skystorm.module.model.RequestQueueModule.requestQueue;
import static com.accedo.android.skystorm.module.model.SimplePreferencesModule.defaultSimplePreferences;
import static com.accedo.android.skystorm.module.model.UrlProviderModule.urlProvider;
import static com.accedo.android.skystorm.module.view.IndicatorModule.logoutIndicator;

/**
 * Handles loging out
 */
public class LogoutFragment extends BaseFragment implements Response.Listener<Boolean>,
        Response.ErrorListener,
        OnLogoutEventListener {

    private RequestQueue requestQueue;
    private final RequestFactory<Boolean> logoutRequestFactory;
    private UrlProvider urlProvider;
    private LogoutIndicator indicator;
    private SimplePreferences simplePreferences;
    private OnLogoutProcessFinishedListener listener;

    public static Fragment newInstance() {
        Fragment result = new LogoutFragment();
        Bundle args = new Bundle();
        result.setArguments(args);
        return result;
    }

    public LogoutFragment() {
        this(requestQueue(),
                simpleRequestFactory(),
                urlProvider(),
                logoutIndicator(),
                defaultSimplePreferences());
    }

    public LogoutFragment(RequestQueue requestQueue, RequestFactory<Boolean> logoutRequestFactory,
                          UrlProvider urlProvider, LogoutIndicator indicator, SimplePreferences simplePreferences) {
        this.requestQueue = requestQueue;
        this.logoutRequestFactory = logoutRequestFactory;
        this.urlProvider = urlProvider;
        this.indicator = indicator;
        this.simplePreferences = simplePreferences;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (OnLogoutProcessFinishedListener)activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_logout, container, false);
        indicator.initialize(view, this);

        return view;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        indicator.showError();
    }

    @Override
    public void onResponse(Boolean response) {
        if (response) {
            simplePreferences.delete(SimplePreferences.SESSION_ID);
            indicator.showSuccessMessage();
            listener.onLogoutProcessFinished();
        } else {
            indicator.showError();
        }
    }

    @Override
    public void onLogoutRequested() {
        requestQueue.add(logoutRequestFactory.createRequest(Request.Method.DELETE,
                urlProvider.sessionUrl(), this,
                this));
    }
}
