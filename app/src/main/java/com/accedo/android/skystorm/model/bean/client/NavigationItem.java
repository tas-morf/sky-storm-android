package com.accedo.android.skystorm.model.bean.client;

import java.util.List;

/**
 * Contains all the necessary information for navigating the app
 */
public class NavigationItem {
    private String title;
    private String url;
    private NavigationItemType type;
    private List<NavigationItem> subNavigationItems;

    private NavigationItem(Builder builder) {
        title = builder.title;
        url = builder.url;
        type = builder.type;
        subNavigationItems = builder.subNavigationItems;
    }

    public static Builder aNavigationItem() {
        return new Builder();
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public NavigationItemType getType() {
        return type;
    }

    public List<NavigationItem> getSubNavigationItems() {
        return subNavigationItems;
    }

    public static final class Builder {
        private String title;
        private String url;
        private NavigationItemType type;
        private List<NavigationItem> subNavigationItems;

        private Builder() {
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public Builder type(NavigationItemType type) {
            this.type = type;
            return this;
        }

        public Builder subNavigationItems(List<NavigationItem> subNavigationItems) {
            this.subNavigationItems = subNavigationItems;
            return this;
        }

        public NavigationItem build() {
            return new NavigationItem(this);
        }
    }
}
