package com.accedo.android.skystorm.model.bean.server;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represents a channel in the server
 */
public class ServerChannel {

    private long id;
    private String name;
    private String color;
    private int hd;
    private String logo;
    private int num;
    private String mobilepc;
    private int ppv;
    @JsonProperty("mediaurl")
    private String mediaUrl;
    private String protection;
    private boolean hasTimedMetadata;
    private int[] socials;

    public ServerChannel() {
        //required by Jackson
    }

    /**
     * Do not use outside tests
     */
    private ServerChannel(Builder builder) {
        id = builder.id;
        name = builder.name;
        color = builder.color;
        hd = builder.hd;
        logo = builder.logo;
        num = builder.num;
        mobilepc = builder.mobilepc;
        ppv = builder.ppv;
        mediaUrl = builder.mediaUrl;
        protection = builder.protection;
        hasTimedMetadata = builder.hasTimedMetadata;
        socials = builder.socials;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public int getHd() {
        return hd;
    }

    public String getLogo() {
        return logo;
    }

    public int getNum() {
        return num;
    }

    public String getMobilepc() {
        return mobilepc;
    }

    public int getPpv() {
        return ppv;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public String getProtection() {
        return protection;
    }

    public boolean isHasTimedMetadata() {
        return hasTimedMetadata;
    }

    public int[] getSocials() {
        return socials;
    }


    public static final class Builder {
        private long id;
        private String name;
        private String color;
        private int hd;
        private String logo;
        private int num;
        private String mobilepc;
        private int ppv;
        private String mediaUrl;
        private String protection;
        private boolean hasTimedMetadata;
        private int[] socials;

        private Builder() {
        }

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder color(String color) {
            this.color = color;
            return this;
        }

        public Builder hd(int hd) {
            this.hd = hd;
            return this;
        }

        public Builder logo(String logo) {
            this.logo = logo;
            return this;
        }

        public Builder num(int num) {
            this.num = num;
            return this;
        }

        public Builder mobilepc(String mobilepc) {
            this.mobilepc = mobilepc;
            return this;
        }

        public Builder ppv(int ppv) {
            this.ppv = ppv;
            return this;
        }

        public Builder mediaUrl(String mediaUrl) {
            this.mediaUrl = mediaUrl;
            return this;
        }

        public Builder protection(String protection) {
            this.protection = protection;
            return this;
        }

        public Builder hasTimedMetadata(boolean hasTimedMetadata) {
            this.hasTimedMetadata = hasTimedMetadata;
            return this;
        }

        public Builder socials(int[] socials) {
            this.socials = socials;
            return this;
        }

        public ServerChannel build() {
            return new ServerChannel(this);
        }
    }
}
