package com.accedo.android.skystorm.model.bean.server;


import java.util.List;

public class ServerChannelsResponse {

    public ServerChannelsResponse() {
        //required by jackson
    }

    private List<ServerChannel> channelList;

    private ServerChannelsResponse(Builder builder) {
        channelList = builder.channelList;
    }

    /**
     * Do not use in code
     */
    public static Builder newBuilder() {
        return new Builder();
    }

    public List<ServerChannel> getChannelList() {
        return channelList;
    }


    public static final class Builder {
        private List<ServerChannel> channelList;

        private Builder() {
        }

        public Builder channelList(List<ServerChannel> channelList) {
            this.channelList = channelList;
            return this;
        }

        public ServerChannelsResponse build() {
            return new ServerChannelsResponse(this);
        }
    }
}
