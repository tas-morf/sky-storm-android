package com.accedo.android.skystorm.view.indicator.impl;

import android.content.res.Resources;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.controller.listener.OnChannelClickListener;
import com.accedo.android.skystorm.model.bean.client.Channel;
import com.accedo.android.skystorm.view.Toaster;
import com.accedo.android.skystorm.view.adapter.MovieAdapter;
import com.accedo.android.skystorm.view.indicator.ChannelListIndicator;

import java.util.List;

/**
 * Uses views in order to diplay a movie list
 */
public class MovieListViewIndicator implements ChannelListIndicator, AdapterView.OnItemClickListener {

    private final Resources res;
    private final Toaster toaster;

    private View progress;
    private ListView listView;
    private OnChannelClickListener onChannelClickListener;

    public MovieListViewIndicator(Resources res, Toaster toaster) {
        this.res = res;
        this.toaster = toaster;
    }

    @Override
    public void initialize(View rootView, OnChannelClickListener onChannelClickListener) {
        this.onChannelClickListener = onChannelClickListener;
        progress = rootView.findViewById(R.id.progress);
        listView = (ListView)rootView.findViewById(R.id.content_list);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void showData(List<Channel> movies) {
        progress.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);
        listView.setAdapter(new MovieAdapter(listView.getContext(), movies));
    }

    @Override
    public void showError() {
        progress.setVisibility(View.GONE);
        //just show a generic error
        toaster.showToast(res.getString(R.string.an_error_happened));
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        onChannelClickListener.onChannelClicked((Channel) adapterView.getAdapter().getItem(i));
    }
}
