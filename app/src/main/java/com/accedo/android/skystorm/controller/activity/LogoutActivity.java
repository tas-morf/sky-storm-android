package com.accedo.android.skystorm.controller.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.accedo.android.skystorm.controller.fragment.LogoutFragment;
import com.accedo.android.skystorm.controller.listener.OnLogoutProcessFinishedListener;
import com.accedo.android.skystorm.controller.service.KeepAliveIntentService;
import com.accedo.android.skystorm.model.bean.client.KeepAliveCommand;

/**
 * Extremely simple class to show the fragment to logout
 */
public class LogoutActivity extends Activity implements OnLogoutProcessFinishedListener {

    public static Intent getLogoutIntent(Context context) {
        return new Intent(context, LogoutActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content,
                LogoutFragment.newInstance()).commit();
    }

    @Override
    public void onLogoutProcessFinished() {
        startService(KeepAliveIntentService.getKeepAliveIntent(this,
                KeepAliveCommand.STOP));
        setResult(RESULT_OK);
        finish();
    }
}
