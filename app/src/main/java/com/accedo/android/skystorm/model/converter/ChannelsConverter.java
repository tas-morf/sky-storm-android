package com.accedo.android.skystorm.model.converter;

import android.content.res.Resources;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.model.bean.client.Channel;
import com.accedo.android.skystorm.model.bean.server.ServerChannel;
import com.accedo.android.skystorm.model.bean.server.ServerChannelsResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Converts from a {@link com.accedo.android.skystorm.model.bean.server.ServerChannelsResponse} to a {@link
 * com.accedo.android.skystorm.model.bean.client.Channel} list
 */
public class ChannelsConverter implements Converter<ServerChannelsResponse, List<Channel>> {

    private Resources resources;

    public ChannelsConverter(Resources resources) {
        this.resources = resources;
    }

    @Override
    public List<Channel> convert(ServerChannelsResponse from) {
        List<ServerChannel> serverChannels = from.getChannelList();
        List<Channel> result = new ArrayList<Channel>(serverChannels.size());
        for (ServerChannel serverChannel : serverChannels) {
            result.add(Channel.aChannel().id(String.valueOf(serverChannel.getId()))
                    .imageUrl(resources.getString(R.string.base_url) + serverChannel.getLogo())
                    .mediaUrl(serverChannel.getMediaUrl())
                    .num(serverChannel.getNum())
                    .title(serverChannel.getName())
                    .build())
            ;
        }
        return result;
    }
}
