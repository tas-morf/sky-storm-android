package com.accedo.android.skystorm.model.converter;

import android.content.res.Resources;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.model.bean.client.SearchResult;
import com.accedo.android.skystorm.model.bean.server.ServerAsset;
import com.accedo.android.skystorm.model.bean.server.ServerSearchResponse;

import java.util.ArrayList;
import java.util.List;

public class SearchConverter implements Converter<ServerSearchResponse, List<SearchResult>> {

    private Resources resources;

    public SearchConverter(Resources resources) {
        this.resources = resources;
    }

    @Override
    public List<SearchResult> convert(ServerSearchResponse from) {
        List<SearchResult> result = new ArrayList<SearchResult>();
        for(ServerAsset serverAsset : from.getAssetListResult()) {
            result.add(SearchResult.aSearchResult()
                    .title(serverAsset.getTitle())
                    .category(serverAsset.getCategory())
                    .id(serverAsset.getId())
                    .imageUrl(resources.getString(R.string.base_url) + serverAsset.getPicture())
                    .isHD(serverAsset.isHd())
                    .parentalRating(serverAsset.getParentalRating().getDisplay())
                    .url(getUrlFrom(serverAsset.getUrl()))
                    .build());
        }
        return result;
    }

    /**
     * Kind of weird way to do it, but doing a json conversion here would be overkill
     * @param serverAssetUrl
     * @return
     */
    private String getUrlFrom(String serverAssetUrl) {
        int start = serverAssetUrl.indexOf("\"ST\":\"");
        String secondHalf = serverAssetUrl.substring(start + 6);
        int end = secondHalf.indexOf("\"");
        return secondHalf.substring(0, end);
    }
}
