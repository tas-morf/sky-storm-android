package com.accedo.android.skystorm.model.converter;

import android.content.res.Resources;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.model.bean.client.NavigationItem;
import com.accedo.android.skystorm.model.bean.client.NavigationItemType;
import com.accedo.android.skystorm.model.bean.server.ServerNavigationNode;
import com.accedo.android.skystorm.model.bean.server.ServerNavigationResponse;
import com.accedo.android.skystorm.model.bean.server.ServerStructureType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NavigationConverter implements Converter<ServerNavigationResponse, List<NavigationItem>> {
    private Resources resources;

    public NavigationConverter(Resources resources) {
        this.resources = resources;
    }


    @Override
    public List<NavigationItem> convert(ServerNavigationResponse from) {
        List<NavigationItem> result = new ArrayList<NavigationItem>();
        List<ServerNavigationNode> level1Nodes = from.getNavigation().getNode();
        for (ServerNavigationNode level1Node : level1Nodes) {
            result.add(handleNode(level1Node));
        }
        return result;
    }

    private NavigationItem handleNode(ServerNavigationNode node) {
        NavigationItem.Builder builder = NavigationItem.aNavigationItem()
                .title(node.getLabel())
                .url(resources.getString(R.string.base_url) + node.getPath())
                .type(getNavigationTypeFor(node.getStructureType()));

        if(node.getNode() != null && !node.getNode().isEmpty()) {
            handleSubNavigationNodes(builder, node.getNode(), hasLicensorNodes(node.getStructureType()));
        } else {
            builder.subNavigationItems(Collections.EMPTY_LIST);
        }
        return builder.build();
    }

    private void handleSubNavigationNodes(NavigationItem.Builder builder, List<ServerNavigationNode> nodes,
                                          boolean hasLicensorNodes) {
        List<NavigationItem> subnavigationItems = new ArrayList<NavigationItem>();
        for (int i = 0; i<nodes.size() ; i++) {
            subnavigationItems.add(handleNode(nodes.get(i)));
            if(hasLicensorNodes) {
                if(nodes.get(i).getStructureType() == ServerStructureType.OrderedListingSection) {
                    //the next item is a licensor
                    NavigationItem.Builder b = NavigationItem.aNavigationItem()
                            .type(NavigationItemType.LICENSORS)
                            .title(resources.getString(R.string.licensors));
                    handleSubNavigationNodes(b, nodes.subList(i+1, nodes.size()), false);
                    subnavigationItems.add(b.build());
                    break;
                }
            }
        }
        builder.subNavigationItems(subnavigationItems);
    }

    private boolean hasLicensorNodes(ServerStructureType structureType) {
        return structureType == ServerStructureType.DocuSection
                || structureType == ServerStructureType.KidsSection
                || structureType == ServerStructureType.SeriesSection;
    }

    private NavigationItemType getNavigationTypeFor(ServerStructureType structureType) {
        switch (structureType) {
            case LandingPageStartScreen:
                return NavigationItemType.HOME;
            case LandingPageEPG:
                return NavigationItemType.SKY_GUIDE;
            case SportSection:
            case FilmSection:
            case SeriesSection:
            case DocuSection:
            case KidsSection:
            case SnapSection:
                return NavigationItemType.TOP_SECTION;
            case LandingPageHighlightsSport:
                return NavigationItemType.SPORT_HIGHLIGHTS;
            case LiveplannerSection:
                return NavigationItemType.LIVE_PLANNER_SECTION;
            case LandingPageLiveplanner:
                return NavigationItemType.LIVE_PLANNER_ITEM;
            case NewsSection:
                return NavigationItemType.SPORTS_NEWS_SECTION;
            case LandingPageSportNews:
                return NavigationItemType.SPORTS_NEWS_ITEM;
            case ClipSection:
                return NavigationItemType.SPORTS_CLIPS_SECTION;
            case LandingPageSportClip:
                return NavigationItemType.SPORTS_CLIPS_ITEM;
            case ReplaysSection:
                return NavigationItemType.SPORT_REPLAYS_SECTION;
            case LandingPageSportReplays:
                return NavigationItemType.SPORT_REPLAYS_ITEM;
            case LandingPageWebView:
                return NavigationItemType.WEBVIEW;
            case LandingPageHighlights:
                return NavigationItemType.HIGHLIGHTS;
            case LandingPageSingleListing:
                return NavigationItemType.SINGLE_LISTING;
            case OrderedListingSection:
                return NavigationItemType.WHOLE_CATALOG;
            case LandingPageOrderedListing:
                return NavigationItemType.SUBCATALOG;
            case LandingPageDoubleListing:
                return NavigationItemType.DOUBLE_LISTING;

            case LandingPageHighlightsSNAP:
            case LandingPageSingleListingSNAP:
            case LandingPageOrderedListingSNAP:
            default:
                return NavigationItemType.SINGLE_LISTING;
        }
    }

}
