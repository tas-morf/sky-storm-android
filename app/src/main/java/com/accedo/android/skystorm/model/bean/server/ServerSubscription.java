package com.accedo.android.skystorm.model.bean.server;

/**
 * Represents a subscription on the server
 */
public class ServerSubscription {
    private String code;
    private String id;
    private String expire;

    public ServerSubscription() {
        //required by jackson
    }

    private ServerSubscription(Builder builder) {
        code = builder.code;
        id = builder.id;
        expire = builder.expire;
    }

    /**
     * Do not use in code
     */
    public static Builder newBuilder() {
        return new Builder();
    }

    public String getCode() {
        return code;
    }

    public String getId() {
        return id;
    }

    public String getExpire() {
        return expire;
    }


    public static final class Builder {
        private String code;
        private String id;
        private String expire;

        private Builder() {
        }

        public Builder code(String code) {
            this.code = code;
            return this;
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder expire(String expire) {
            this.expire = expire;
            return this;
        }

        public ServerSubscription build() {
            return new ServerSubscription(this);
        }
    }
}
