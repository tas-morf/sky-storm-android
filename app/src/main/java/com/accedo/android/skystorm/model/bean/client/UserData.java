package com.accedo.android.skystorm.model.bean.client;


import java.util.List;

public class UserData {

    private String country;
    private String email;
    private String customerId;
    private String customerCode;
    private String presentation;
    private String name;
    private boolean tcFlag;
    private boolean privacyFlag;
    private String ccExpire;
    private int dunning;
    private boolean tvod;
    private List<String> entitlements;
    private List<String> dayPasses;
    private List<String> subscriptions;
    private List<String> addOns;

    private UserData(Builder builder) {
        country = builder.country;
        email = builder.email;
        customerId = builder.customerId;
        customerCode = builder.customerCode;
        presentation = builder.presentation;
        name = builder.name;
        tcFlag = builder.tcFlag;
        privacyFlag = builder.privacyFlag;
        ccExpire = builder.ccExpire;
        dunning = builder.dunning;
        tvod = builder.tvod;
        entitlements = builder.entitlements;
        dayPasses = builder.dayPasses;
        subscriptions = builder.subscriptions;
        addOns = builder.addOns;
    }

    public static Builder aUserData() {
        return new Builder();
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getCountry() {
        return country;
    }

    public String getEmail() {
        return email;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getPresentation() {
        return presentation;
    }

    public String getName() {
        return name;
    }

    public boolean isTcFlag() {
        return tcFlag;
    }

    public boolean isPrivacyFlag() {
        return privacyFlag;
    }

    public String getCcExpire() {
        return ccExpire;
    }

    public int getDunning() {
        return dunning;
    }

    public boolean isTvod() {
        return tvod;
    }

    public List<String> getEntitlements() {
        return entitlements;
    }

    public List<String> getDayPasses() {
        return dayPasses;
    }

    public List<String> getSubscriptions() {
        return subscriptions;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public List<String> getAddOns() {
        return addOns;
    }


    public static final class Builder {
        private String country;
        private String email;
        private String customerId;
        private String customerCode;
        private String presentation;
        private String name;
        private boolean tcFlag;
        private boolean privacyFlag;
        private String ccExpire;
        private int dunning;
        private boolean tvod;
        private List<String> entitlements;
        private List<String> dayPasses;
        private List<String> subscriptions;
        private List<String> addOns;

        private Builder() {
        }

        public Builder country(String country) {
            this.country = country;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder customerId(String customerId) {
            this.customerId = customerId;
            return this;
        }

        public Builder customerCode(String customerCode) {
            this.customerCode = customerCode;
            return this;
        }

        public Builder presentation(String presentation) {
            this.presentation = presentation;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder tcFlag(boolean tcFlag) {
            this.tcFlag = tcFlag;
            return this;
        }

        public Builder privacyFlag(boolean privacyFlag) {
            this.privacyFlag = privacyFlag;
            return this;
        }

        public Builder ccExpire(String ccExpire) {
            this.ccExpire = ccExpire;
            return this;
        }

        public Builder dunning(int dunning) {
            this.dunning = dunning;
            return this;
        }

        public Builder tvod(boolean tvod) {
            this.tvod = tvod;
            return this;
        }

        public Builder entitlements(List<String> entitlements) {
            this.entitlements = entitlements;
            return this;
        }

        public Builder dayPasses(List<String> dayPasses) {
            this.dayPasses = dayPasses;
            return this;
        }

        public Builder subscriptions(List<String> subscriptions) {
            this.subscriptions = subscriptions;
            return this;
        }

        public Builder addOns(List<String> addOns) {
            this.addOns = addOns;
            return this;
        }

        public UserData build() {
            return new UserData(this);
        }
    }
}
