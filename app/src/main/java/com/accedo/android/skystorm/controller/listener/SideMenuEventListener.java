package com.accedo.android.skystorm.controller.listener;

import com.accedo.android.skystorm.model.bean.client.NavigationItem;

/**
 * Listens for events on the side menu
 */
public interface SideMenuEventListener {
    void onNavigationItemSelected(NavigationItem navItem);

    void onSettingsClicked();

    void onLoginClicked();

    void onLogoutClicked();
}
