package com.accedo.android.skystorm.module;

import android.content.res.Resources;

import static com.accedo.android.skystorm.module.ApplicationModule.applicationContext;

public class ResourcesModule {
    public static Resources resources() {
        return applicationContext().getResources();
    }
}
