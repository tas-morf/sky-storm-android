package com.accedo.android.skystorm.view.indicator.impl;

import android.view.View;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.controller.listener.OnLogoutEventListener;
import com.accedo.android.skystorm.view.Toaster;
import com.accedo.android.skystorm.view.indicator.LogoutIndicator;

public class LogoutViewIndicator implements LogoutIndicator, View.OnClickListener {
    private Toaster toaster;
    private OnLogoutEventListener listener;

    public LogoutViewIndicator(Toaster toaster) {
        this.toaster = toaster;
    }

    @Override
    public void initialize(View view, OnLogoutEventListener listener) {
        this.listener = listener;
        view.findViewById(R.id.logout_button).setOnClickListener(this);
    }

    @Override
    public void showError() {
        toaster.showToast("Logout could not be completed!");
    }

    @Override
    public void showSuccessMessage() {
        toaster.showToast("Logout Complete!");
    }

    @Override
    public void onClick(View view) {
        listener.onLogoutRequested();
    }
}
