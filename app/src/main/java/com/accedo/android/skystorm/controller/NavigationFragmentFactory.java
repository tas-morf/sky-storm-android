package com.accedo.android.skystorm.controller;

import android.app.Fragment;

import com.accedo.android.skystorm.model.bean.client.NavigationItem;

/**
 * Creates fragments based on a navigation item
 */
public interface NavigationFragmentFactory {
    Fragment fragmentFor(NavigationItem navItem);
}
