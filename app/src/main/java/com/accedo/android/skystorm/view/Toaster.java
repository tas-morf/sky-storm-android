package com.accedo.android.skystorm.view;

/**
 * Shows toasts
 */
public interface Toaster {

    void showToast(String toastMessage);

    void showToast(int stringResource);
}
