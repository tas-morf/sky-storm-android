package com.accedo.android.skystorm.view;

import android.content.Context;
import android.widget.Toast;

/**
 * Shows toasts using the regular {@link android.widget.Toast}
 */
public class AndroidToaster implements Toaster {
    private Context context;

    public AndroidToaster(Context context) {
        this.context = context;
    }

    @Override
    public void showToast(String toastMessage) {
        Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(int stringResource) {
        Toast.makeText(context, stringResource, Toast.LENGTH_SHORT).show();
    }
}
