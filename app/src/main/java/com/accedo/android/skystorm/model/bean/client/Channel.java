package com.accedo.android.skystorm.model.bean.client;

/**
 * Represents a single channel
 */
public class Channel {

    private String title;
    private String id;
    private String mediaUrl;
    private String imageUrl;
    private int num;

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public int getNum() {
        return num;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    private Channel(Builder builder) {
        title = builder.title;
        id = builder.id;
        mediaUrl = builder.mediaUrl;
        imageUrl = builder.imageUrl;
        num = builder.num;
    }

    public static Builder aChannel() {
        return new Builder();
    }

    public static final class Builder {
        private String title;
        private String id;
        private String mediaUrl;
        private String imageUrl;
        private int num;

        private Builder() {
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder mediaUrl(String mediaUrl) {
            this.mediaUrl = mediaUrl;
            return this;
        }

        public Builder imageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
            return this;
        }

        public Builder num(int num) {
            this.num = num;
            return this;
        }

        public Channel build() {
            return new Channel(this);
        }
    }
}
