package com.accedo.android.skystorm.view.indicator.impl;

import android.content.res.Resources;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.controller.listener.OnSearchListener;
import com.accedo.android.skystorm.model.bean.client.SearchResult;
import com.accedo.android.skystorm.view.Toaster;
import com.accedo.android.skystorm.view.adapter.SearchAdapter;
import com.accedo.android.skystorm.view.indicator.SearchIndicator;

import java.util.List;

public class SearchViewIndicator implements SearchIndicator, SearchView.OnQueryTextListener {

    private OnSearchListener listener;
    private AbsListView searchListView;
    private SearchAdapter searchAdapter;
    private TextView subitleText;
    private Resources res;
    private Toaster toaster;
    private SearchView searchView;

    public SearchViewIndicator(Resources res, Toaster toaster) {
        this.res = res;
        this.toaster = toaster;
    }

    @Override
    public void initialize(View rootView, OnSearchListener listener) {
        this.listener = listener;
        searchListView = (AbsListView)rootView.findViewById(R.id.search_list);
        subitleText = (TextView)rootView.findViewById(R.id.subtitle);
        searchAdapter = new SearchAdapter(rootView.getContext());
        searchListView.setAdapter(searchAdapter);
    }

    @Override
    public void modifySearchItem(MenuItem item) {
        searchView = (SearchView) item.getActionView();
        searchView.setIconifiedByDefault(false);
        searchView.setIconified(false);
        searchView.onActionViewExpanded();
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public void showError() {
        toaster.showToast(R.string.unable_to_search);
    }

    @Override
    public void showData(List<SearchResult> response) {
        subitleText.setText(res.getString(R.string.search_results, searchView.getQuery(), response.size()));
        searchAdapter.setData(response);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        listener.onSearch(s);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }
}
