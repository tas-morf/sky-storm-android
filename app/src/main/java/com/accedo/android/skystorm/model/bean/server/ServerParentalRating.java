package com.accedo.android.skystorm.model.bean.server;

public class ServerParentalRating {
    private int value;
    private String display;

    public int getValue() {
        return value;
    }

    public String getDisplay() {
        return display;
    }
}
