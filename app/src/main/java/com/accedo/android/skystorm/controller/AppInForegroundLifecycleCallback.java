package com.accedo.android.skystorm.controller;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

/**
 * Implements the activity lifecycle callbacks in order to be able to determine whether one of this application's
 * activities is currently in the foreground.
 */
public class AppInForegroundLifecycleCallback implements Application.ActivityLifecycleCallbacks, AppInForegroundChecker {

    //number of resumed activities
    private int resumed;

    //number of paused activities
    private int paused;

    //number of stopped activities
    private int stopped;

    //number of started activities
    private int started;

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityStarted(Activity activity) {
        started++;
    }

    @Override
    public void onActivityResumed(Activity activity) {
        resumed++;
    }

    @Override
    public void onActivityPaused(Activity activity) {
        paused++;
    }

    @Override
    public void onActivityStopped(Activity activity) {
        stopped++;
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    @Override
    public boolean isAppVisible() {
        return started > stopped;
    }

    @Override
    public boolean isAppInForeground() {
        return resumed > paused;
    }
}
