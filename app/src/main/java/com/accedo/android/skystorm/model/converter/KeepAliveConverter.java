package com.accedo.android.skystorm.model.converter;

import com.accedo.android.skystorm.model.bean.client.KeepAlive;
import com.accedo.android.skystorm.model.bean.server.ServerKeepAliveResponse;

public class KeepAliveConverter implements Converter<ServerKeepAliveResponse, KeepAlive> {

    @Override
    public KeepAlive convert(ServerKeepAliveResponse from) {
        return KeepAlive.aKeepAlive()
                .success("OK".equals(from.getResultMessage()))
                .reloadUserData(from.isReloadUserData())
                .build();
    }
}
