package com.accedo.android.skystorm.model.bean.client;

/**
 * Contains all the possible types for navigation items
 */
public enum NavigationItemType {
    HOME, //the home screen
    SKY_GUIDE, //sky guide, epg etc
    TOP_SECTION, //a section, like films, series, documentaries etc
    SPORT_HIGHLIGHTS, //the first page for sports. it is unique, so it needs a different type
    LIVE_PLANNER_SECTION, //live planner only exists in sports for now. this is a section, that contains more items.
    LIVE_PLANNER_ITEM, //each item in the live planner has this type
    SPORTS_NEWS_SECTION, //only used by sports for now
    SPORTS_NEWS_ITEM, //only used by sports for now
    SPORTS_CLIPS_SECTION, //only used by sports
    SPORTS_CLIPS_ITEM, //only used by sports
    SPORT_REPLAYS_SECTION, //only used by sports
    SPORT_REPLAYS_ITEM, //only used by sports
    WEBVIEW, //only used by the dataplanner in sports
    HIGHLIGHTS, //highlights of any section (like films, series, etc)
    WHOLE_CATALOG, //the whole catalog of a section (all movies, all series etc)
    SUBCATALOG, //a subsection of the whole catalog (action films, comedy films etc)
    SINGLE_LISTING, //a single listing page, containing either movies, or episodes
    DOUBLE_LISTING, //a double listing page, containing both movies and episodes
    LICENSORS //a list of horizontally scrolled sections, each representing a licensor
}
