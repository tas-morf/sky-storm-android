package com.accedo.android.skystorm.module.view;

import com.accedo.android.skystorm.view.AndroidToaster;
import com.accedo.android.skystorm.view.Toaster;

import static com.accedo.android.skystorm.module.ApplicationModule.applicationContext;

public class ToasterModule {

    public static Toaster toaster() {
        return new AndroidToaster(applicationContext());
    }
}
