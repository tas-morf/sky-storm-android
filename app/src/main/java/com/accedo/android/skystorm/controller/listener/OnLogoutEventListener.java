package com.accedo.android.skystorm.controller.listener;

/**
 * Hears for logout events
 */
public interface OnLogoutEventListener {
    void onLogoutRequested();
}
