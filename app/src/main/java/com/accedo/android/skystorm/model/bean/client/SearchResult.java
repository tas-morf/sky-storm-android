package com.accedo.android.skystorm.model.bean.client;

public class SearchResult {
    private long id;
    private String title;
    private String url;
    private String category;
    private String imageUrl;
    private String parentalRating;
    private boolean isHD;

    private SearchResult(Builder builder) {
        id = builder.id;
        title = builder.title;
        url = builder.url;
        category = builder.category;
        imageUrl = builder.imageUrl;
        parentalRating = builder.parentalRating;
        isHD = builder.isHD;
    }

    public static Builder aSearchResult() {
        return new Builder();
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getCategory() {
        return category;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getParentalRating() {
        return parentalRating;
    }

    public boolean isHD() {
        return isHD;
    }

    public static final class Builder {
        private long id;
        private String title;
        private String url;
        private String category;
        private String imageUrl;
        private String parentalRating;
        private boolean isHD;

        private Builder() {
        }

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public Builder category(String category) {
            this.category = category;
            return this;
        }

        public Builder imageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
            return this;
        }

        public Builder parentalRating(String parentalRating) {
            this.parentalRating = parentalRating;
            return this;
        }

        public Builder isHD(boolean isHD) {
            this.isHD = isHD;
            return this;
        }

        public SearchResult build() {
            return new SearchResult(this);
        }
    }
}
