package com.accedo.android.skystorm.module.controller;

import android.app.Application;

import com.accedo.android.skystorm.controller.AppInForegroundChecker;
import com.accedo.android.skystorm.controller.AppInForegroundLifecycleCallback;

public class AppInForegroundCheckerModule {

    private static AppInForegroundLifecycleCallback appInForegroundChecker = new AppInForegroundLifecycleCallback();

    public static AppInForegroundChecker appInForegroundChecker() {
        return appInForegroundChecker;
    }

    public static Application.ActivityLifecycleCallbacks appInForegroundLifecycleCallback() {
        return appInForegroundChecker;
    }
}
