package com.accedo.android.skystorm.model;

import com.accedo.android.skystorm.model.bean.client.LoginCredentials;

/**
 * Provides urls (we can add methods here that take url parameters etc)
 */
public interface UrlProvider {
    String allChannelsUrl();

    String loginUrl(LoginCredentials loginCredentials);

    String sessionUrl();

    String userDataUrl();

    //pass the session ID, since it hasn't been saved in the preferences yet
    String sessionKillUrl(String sessionId);

    //pass the session ID, since it hasn't been saved in the preferences yet
    String getAcceptTermsUrl(String sessionId);

    String navigationUrl();

    String searchUrl(String searchTerm);
}
