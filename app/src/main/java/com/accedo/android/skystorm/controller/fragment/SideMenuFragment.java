package com.accedo.android.skystorm.controller.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.controller.listener.SideMenuEventListener;
import com.accedo.android.skystorm.model.UrlProvider;
import com.accedo.android.skystorm.model.bean.client.NavigationItem;
import com.accedo.android.skystorm.model.persistence.SimplePreferences;
import com.accedo.android.skystorm.model.request.RequestFactory;
import com.accedo.android.skystorm.view.indicator.SideMenuIndicator;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.List;

import static com.accedo.android.skystorm.module.model.RequestFactoryModule.navigationRequestFactory;
import static com.accedo.android.skystorm.module.model.RequestQueueModule.requestQueue;
import static com.accedo.android.skystorm.module.model.SimplePreferencesModule.defaultSimplePreferences;
import static com.accedo.android.skystorm.module.model.UrlProviderModule.urlProvider;
import static com.accedo.android.skystorm.module.view.IndicatorModule.sideMenuIndicator;

/**
 * Contains the left-side navigation menu
 */
public class SideMenuFragment extends Fragment implements Response.ErrorListener,
        Response.Listener<List<NavigationItem>> {

    private SideMenuIndicator indicator;
    private RequestQueue requestQueue;
    private RequestFactory<List<NavigationItem>> navigationRequestFactory;
    private UrlProvider urlProvider;
    private SimplePreferences simplePreferences;
    private SideMenuEventListener sideMenuEventListener;

    public SideMenuFragment() {
        this(sideMenuIndicator(), requestQueue(), navigationRequestFactory(), urlProvider(), defaultSimplePreferences());
    }

    public SideMenuFragment(SideMenuIndicator indicator,
                            RequestQueue requestQueue,
                            RequestFactory<List<NavigationItem>> navigationRequestFactory,
                            UrlProvider urlProvider,
                            SimplePreferences simplePreferences) {
        this.indicator = indicator;
        this.requestQueue = requestQueue;
        this.navigationRequestFactory = navigationRequestFactory;
        this.urlProvider = urlProvider;
        this.simplePreferences = simplePreferences;
    }

    public static Fragment newInstance() {
        Fragment result = new SideMenuFragment();
        Bundle args = new Bundle();
        result.setArguments(args);
        return result;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        sideMenuEventListener = (SideMenuEventListener)activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_side_menu, container, false);
        indicator.initialize(view, sideMenuEventListener);
        refreshData();
        return view;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        indicator.showError();
    }

    @Override
    public void onResponse(List<NavigationItem> response) {
        indicator.showSideMenu(response, simplePreferences.contains(SimplePreferences.SESSION_ID));
        sideMenuEventListener.onNavigationItemSelected(response.get(0));
    }

    public void refreshData() {
        requestQueue.add(navigationRequestFactory.createRequest(urlProvider.navigationUrl(), this, this));
    }
}
