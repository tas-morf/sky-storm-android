package com.accedo.android.skystorm.model.bean.server;

public class ServerNavigationResponse {
    private ServerNavigation navigation;
    private String product;

    public ServerNavigationResponse() {
        //required by jackson
    }

    private ServerNavigationResponse(Builder builder) {
        product = builder.product;
        navigation = builder.navigation;
    }

    /**
     * Not to be used outside tests
     */
    public static Builder newBuilder() {
        return new Builder();
    }

    public ServerNavigation getNavigation() {
        return navigation;
    }

    public String getProduct() {
        return product;
    }


    public static final class Builder {
        private String product;
        private ServerNavigation navigation;

        private Builder() {
        }

        public Builder product(String product) {
            this.product = product;
            return this;
        }

        public Builder navigation(ServerNavigation navigation) {
            this.navigation = navigation;
            return this;
        }

        public ServerNavigationResponse build() {
            return new ServerNavigationResponse(this);
        }
    }
}
