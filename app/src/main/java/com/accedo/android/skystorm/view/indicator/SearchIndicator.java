package com.accedo.android.skystorm.view.indicator;

import android.view.MenuItem;
import android.view.View;

import com.accedo.android.skystorm.controller.listener.OnSearchListener;
import com.accedo.android.skystorm.model.bean.client.SearchResult;

import java.util.List;

/**
 * Handles all the UI for the search function
 */
public interface SearchIndicator {

    void modifySearchItem(MenuItem item);

    void initialize(View rootView, OnSearchListener listener);

    void showError();

    void showData(List<SearchResult> response);
}
