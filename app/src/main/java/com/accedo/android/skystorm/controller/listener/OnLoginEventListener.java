package com.accedo.android.skystorm.controller.listener;

import com.accedo.android.skystorm.model.bean.client.LoginCredentials;

/**
 * Listens for UI events in the login screen
 */
public interface OnLoginEventListener {

    void onLoginRequested(LoginCredentials loginCredentials);
}
