package com.accedo.android.skystorm.controller.listener;

import com.accedo.android.skystorm.model.bean.client.Channel;

/**
 * Listens for movie clicking events
 */
public interface OnChannelClickListener {
    void onChannelClicked(Channel channel);
}
