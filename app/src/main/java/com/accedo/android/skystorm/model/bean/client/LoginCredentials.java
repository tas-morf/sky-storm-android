package com.accedo.android.skystorm.model.bean.client;

/**
 * Represents the credentials entered by the user during login
 */
public class LoginCredentials {
    private String customerId;
    private String email;
    private String password;

    private LoginCredentials(Builder builder) {
        customerId = builder.customerId;
        email = builder.email;
        password = builder.password;
    }

    public static Builder aLoginCredentials() {
        return new Builder();
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public static final class Builder {
        private String customerId;
        private String email;
        private String password;

        private Builder() {
        }

        public Builder customerId(String customerId) {
            this.customerId = customerId;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public LoginCredentials build() {
            return new LoginCredentials(this);
        }
    }
}
