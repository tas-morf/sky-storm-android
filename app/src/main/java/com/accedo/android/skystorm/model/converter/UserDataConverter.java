package com.accedo.android.skystorm.model.converter;

import com.accedo.android.skystorm.model.bean.client.UserData;
import com.accedo.android.skystorm.model.bean.server.ServerUserData;

public class UserDataConverter implements Converter<ServerUserData, UserData> {
    @Override
    public UserData convert(ServerUserData from) {
        return UserData.aUserData()
                .tvod(from.isTvod())
                .ccExpire(from.getCcExpire())
                .country(from.getCountry())
                .customerId(from.getCustomerId())
                .dayPasses(from.getDayPasses())
                .email(from.getEmail())
                .dunning(from.getDunning())
                .name(from.getFirstName() + " " + from.getLastName())
                .presentation(from.getPresentation())
                .customerCode(from.getCustomerCode())
                .build();
    }
}
