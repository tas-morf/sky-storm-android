package com.accedo.android.skystorm.model.bean.server;

public enum ServerAssetContentType {
    Episode,
    Film
}
