package com.accedo.android.skystorm.controller.listener;

/**
 * Gets notified when the login process has finished successfully
 */
public interface OnLoginProcessFinishedListener {
    void onLoginProcessFinished();
}
