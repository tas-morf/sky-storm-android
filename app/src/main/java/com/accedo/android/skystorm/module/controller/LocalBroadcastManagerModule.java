package com.accedo.android.skystorm.module.controller;

import android.support.v4.content.LocalBroadcastManager;

import static com.accedo.android.skystorm.module.ApplicationModule.applicationContext;

public class LocalBroadcastManagerModule {
    public static LocalBroadcastManager localBroadcastManager() {
        return LocalBroadcastManager.getInstance(applicationContext());
    }
}
