package com.accedo.android.skystorm.controller.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.accedo.android.skystorm.controller.fragment.LoginFragment;
import com.accedo.android.skystorm.controller.listener.OnLoginProcessFinishedListener;
import com.accedo.android.skystorm.controller.service.KeepAliveIntentService;
import com.accedo.android.skystorm.model.bean.client.KeepAliveCommand;
import com.accedo.android.skystorm.model.persistence.SimplePreferences;

import static com.accedo.android.skystorm.module.model.SimplePreferencesModule.defaultSimplePreferences;

/**
 * A simple shell activity for now, that simply uses a fragment
 */
public class LoginActivity extends Activity implements OnLoginProcessFinishedListener {

    private SimplePreferences simplePreferences;

    public static Intent getLoginActivityIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    public LoginActivity() {
        this(defaultSimplePreferences());
    }

    public LoginActivity(SimplePreferences simplePreferences) {
        this.simplePreferences = simplePreferences;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (simplePreferences.getString(SimplePreferences.SESSION_ID) != null) {
            onLoginProcessFinished();
        } else {
            getFragmentManager().beginTransaction().replace(android.R.id.content,
                    LoginFragment.newInstance()).commit();
        }
    }

    @Override
    public void onLoginProcessFinished() {
        startService(KeepAliveIntentService.getKeepAliveIntent(this,
                KeepAliveCommand.START));
        setResult(RESULT_OK);
        finish();
    }
}
