package com.accedo.android.skystorm.controller.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.controller.activity.TermsAndConditionsActivity;
import com.accedo.android.skystorm.controller.listener.OnLoginEventListener;
import com.accedo.android.skystorm.controller.listener.OnLoginProcessFinishedListener;
import com.accedo.android.skystorm.model.UrlProvider;
import com.accedo.android.skystorm.model.bean.client.LoginCredentials;
import com.accedo.android.skystorm.model.bean.client.Session;
import com.accedo.android.skystorm.model.bean.client.SessionError;
import com.accedo.android.skystorm.model.persistence.SimplePreferences;
import com.accedo.android.skystorm.model.request.RequestFactory;
import com.accedo.android.skystorm.module.model.RequestFactoryModule;
import com.accedo.android.skystorm.view.indicator.LoginIndicator;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import static com.accedo.android.skystorm.module.model.RequestFactoryModule.loginRequestFactory;
import static com.accedo.android.skystorm.module.model.RequestFactoryModule.simpleRequestFactory;
import static com.accedo.android.skystorm.module.model.RequestQueueModule.requestQueue;
import static com.accedo.android.skystorm.module.model.SimplePreferencesModule.defaultSimplePreferences;
import static com.accedo.android.skystorm.module.model.UrlProviderModule.urlProvider;
import static com.accedo.android.skystorm.module.view.IndicatorModule.loginIndicator;


/**
 * This fragment handles the log in functionality
 */
public class LoginFragment extends BaseFragment implements Response.Listener<Session>,
        Response.ErrorListener,
        OnLoginEventListener {

    private static final int TNC_REQUEST_CODE = 100;
    private final RequestQueue requestQueue;
    private final RequestFactory<Session> sessionRequestFactory;
    private final RequestFactory<Boolean> sessionKillingRequestFactory;
    private final UrlProvider urlProvider;
    private final LoginIndicator indicator;
    private final SimplePreferences simplePreferences;
    private OnLoginProcessFinishedListener listener;

    public static Fragment newInstance() {
        Fragment result = new LoginFragment();
        Bundle args = new Bundle();
        result.setArguments(args);
        return result;
    }

    public LoginFragment() {
        this(requestQueue(),
                loginRequestFactory(),
                simpleRequestFactory(),
                urlProvider(),
                loginIndicator(),
                defaultSimplePreferences());
    }

    public LoginFragment(RequestQueue requestQueue,
                         RequestFactory<Session> sessionRequestFactory,
                         RequestFactory<Boolean> sessionKillingRequestFactory,
                         UrlProvider urlProvider,
                         LoginIndicator indicator,
                         SimplePreferences simplePreferences) {
        this.requestQueue = requestQueue;
        this.sessionRequestFactory = sessionRequestFactory;
        this.sessionKillingRequestFactory = sessionKillingRequestFactory;
        this.urlProvider = urlProvider;
        this.indicator = indicator;
        this.simplePreferences = simplePreferences;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (OnLoginProcessFinishedListener)activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        indicator.initialize(view, this);

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == TNC_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            //TODO: Not sure about this, we might not need to do the request again,
            //and just go to the next acitvity (after saving the session id of course
            onLoginRequested(indicator.getLoginCredentials());
        }
    }

    @Override
    public void onLoginRequested(LoginCredentials loginCredentials) {
        requestQueue.add(sessionRequestFactory.createRequest(Request.Method.POST, urlProvider.loginUrl(loginCredentials), this, this));
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        indicator.showNetworkError();
    }

    @Override
    public void onResponse(Session response) {
        if(response.isError()) {
            handleSessionError(response);
        } else {
            simplePreferences.saveString(SimplePreferences.SESSION_ID, response.getSessionId());
            indicator.showGreeting(response.getName());
            listener.onLoginProcessFinished();
        }
    }

    private void handleSessionError(Session session) {
        switch (session.getSessionError()) {
            case NO_TNC_ACCEPTED:
                startActivityForResult(TermsAndConditionsActivity.getTermsAndConditionsIntent(getActivityOveride(),
                        session.getSessionId()), TNC_REQUEST_CODE);
                break;
            case ALREADY_LOGGED_IN:
                requestQueue.add(sessionKillingRequestFactory.createRequest(Request.Method.POST,
                        urlProvider.sessionKillUrl(session.getSessionId()),
                        sessionKillListener, this));
                break;
        }
        indicator.showServerError(session.getSessionError(), session.getRemainingAttempts());
    }
    private void onSessionKilled(boolean response) {
        if(response) {
            onLoginRequested(indicator.getLoginCredentials());
        } else {
            indicator.sessionKillFailed();
        }
    }

    private Response.Listener<Boolean> sessionKillListener = new Response.Listener<Boolean>() {
        @Override
        public void onResponse(Boolean response) {
            onSessionKilled(response);
        }
    };

}
