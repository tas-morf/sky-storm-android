package com.accedo.android.skystorm.module.model;

import com.accedo.android.skystorm.model.bean.client.Channel;
import com.accedo.android.skystorm.model.bean.client.KeepAlive;
import com.accedo.android.skystorm.model.bean.client.NavigationItem;
import com.accedo.android.skystorm.model.bean.client.SearchResult;
import com.accedo.android.skystorm.model.bean.client.Session;
import com.accedo.android.skystorm.model.bean.client.UserData;
import com.accedo.android.skystorm.model.bean.server.ServerChannelsResponse;
import com.accedo.android.skystorm.model.bean.server.ServerKeepAliveResponse;
import com.accedo.android.skystorm.model.bean.server.ServerNavigationResponse;
import com.accedo.android.skystorm.model.bean.server.ServerSearchResponse;
import com.accedo.android.skystorm.model.bean.server.ServerSimpleResponse;
import com.accedo.android.skystorm.model.bean.server.ServerUserData;
import com.accedo.android.skystorm.model.request.JsonRequestFactory;
import com.accedo.android.skystorm.model.request.RequestFactory;

import java.util.List;

import static com.accedo.android.skystorm.module.model.ConverterModule.channelConverter;
import static com.accedo.android.skystorm.module.model.ConverterModule.keepAliveConverter;
import static com.accedo.android.skystorm.module.model.ConverterModule.navigationConverter;
import static com.accedo.android.skystorm.module.model.ConverterModule.simpleConverter;
import static com.accedo.android.skystorm.module.model.ConverterModule.sessionConverter;
import static com.accedo.android.skystorm.module.model.ConverterModule.userDataConverter;
import static com.accedo.android.skystorm.module.model.HeaderProviderModule.headerProvider;
import static com.accedo.android.skystorm.module.model.ObjectMapperModule.objectMapper;

public class RequestFactoryModule {

    public static RequestFactory<List<Channel>> allChannelsRequestFactory() {
        return new JsonRequestFactory<ServerChannelsResponse, List<Channel>>(headerProvider(),
                objectMapper(),
                channelConverter(),
                ServerChannelsResponse.class);
    }

    public static RequestFactory<Session> loginRequestFactory() {
        return new JsonRequestFactory<ServerUserData, Session>(headerProvider(),
                objectMapper(),
                sessionConverter(),
                ServerUserData.class);
    }

    public static RequestFactory<Boolean> simpleRequestFactory() {
        return new JsonRequestFactory<ServerSimpleResponse, Boolean>(headerProvider(),
                objectMapper(),
                simpleConverter(),
                ServerSimpleResponse.class);
    }

    public static RequestFactory<KeepAlive> keepAliveRequestFactory() {
        return new JsonRequestFactory<ServerKeepAliveResponse, KeepAlive>(headerProvider(),
                objectMapper(),
                keepAliveConverter(),
                ServerKeepAliveResponse.class);
    }

    public static RequestFactory<UserData> userDataRequestFactory() {
        return new JsonRequestFactory<ServerUserData, UserData>(headerProvider(),
                objectMapper(),
                userDataConverter(),
                ServerUserData.class);
    }

    public static RequestFactory<List<NavigationItem>> navigationRequestFactory() {
        return new JsonRequestFactory<ServerNavigationResponse, List<NavigationItem>>(headerProvider(),
                objectMapper(),
                navigationConverter(),
                ServerNavigationResponse.class);
    }

    public static RequestFactory<List<SearchResult>> searchRequestFactory() {
        return new JsonRequestFactory<ServerSearchResponse, List<SearchResult>>(headerProvider(),
                objectMapper(),
                ConverterModule.searchConverter(),
                ServerSearchResponse.class);
    }
}
