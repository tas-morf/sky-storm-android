package com.accedo.android.skystorm.model.bean.client;

import com.accedo.android.skystorm.R;

/**
 * Contains all the different types of errors that might come back while doing session calls
 */
public enum SessionError {
    NO_TNC_ACCEPTED(R.string.no_tnc_accepted),
    SESSION_BLOCKED(R.string.session_blocked),
    WRONG_PIN(R.string.wrong_pin),
    ACCOUNT_LOCKED(R.string.account_locked),
    SPECIAL_VALIDATION(R.string.special_validation),
    ALREADY_LOGGED_IN(R.string.already_logged_in),
    CUSTOMER_NOT_FOUND(R.string.customer_not_found),
    SERVER_ERROR(R.string.server_error);

    private int stringId;

    SessionError(int stringId) {

        this.stringId = stringId;
    }

    public int getErrorStringId() {
        return stringId;
    }

    public static SessionError from(int errorCode) {
        switch (errorCode) {
            case 215:
                return NO_TNC_ACCEPTED;
            case 200:
                return SESSION_BLOCKED;
            case 201:
                return WRONG_PIN;
            case 202:
                return ACCOUNT_LOCKED;
            case 205:
                return SPECIAL_VALIDATION;
            case 206:
                return ALREADY_LOGGED_IN;
            case 207:
                return CUSTOMER_NOT_FOUND;
            case 223:
            case 242:
            case 998:
            case 999:
            default:
                return SERVER_ERROR;
        }
    }

}
