package com.accedo.android.skystorm;

import android.app.Application;
import android.os.Build;

import com.accedo.android.skystorm.model.persistence.SimplePreferences;
import com.accedo.android.skystorm.module.ApplicationModule;

import java.util.UUID;

import static com.accedo.android.skystorm.module.controller.AppInForegroundCheckerModule.appInForegroundLifecycleCallback;
import static com.accedo.android.skystorm.module.model.SimplePreferencesModule.defaultSimplePreferences;

public class SkystormApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ApplicationModule.setApplication(this);
        generateUniqueId(defaultSimplePreferences());
        //in order to know whether the app is in the foreground
        registerActivityLifecycleCallbacks(appInForegroundLifecycleCallback());
    }

    /**
     * Generates a unique id for this device and stores it in the preferences. If it already exists, no need to
     * create another one.
     * @param simplePreferences
     */
    private void generateUniqueId(SimplePreferences simplePreferences) {
        String deviceId = simplePreferences.getString(SimplePreferences.DEVICE_ID);
        if(deviceId == null) {
            //no device ID setup, create one
            deviceId = UUID.randomUUID().toString();
            simplePreferences.saveString(SimplePreferences.DEVICE_ID, deviceId);
            simplePreferences.saveString(SimplePreferences.DEVICE_FRIENDLY_NAME, Build.MODEL + " " + Build.SERIAL);
        }
    }


}
