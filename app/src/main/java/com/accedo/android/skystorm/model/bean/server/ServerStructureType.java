package com.accedo.android.skystorm.model.bean.server;

/**
 * Has all the different structure types for navigation elements
 */
public enum ServerStructureType {
    LandingPageStartScreen,
    LandingPageEPG,
    SportSection,
    LandingPageHighlightsSport,
    LiveplannerSection,
    LandingPageLiveplanner,
    NewsSection,
    LandingPageSportNews,
    ClipSection,
    LandingPageSportClip,
    ReplaysSection,
    LandingPageSportReplays,
    LandingPageWebView,
    FilmSection,
    LandingPageHighlights,
    LandingPageSingleListing,
    OrderedListingSection,
    LandingPageOrderedListing,
    SeriesSection,
    DocuSection,
    LandingPageDoubleListing,
    KidsSection,
    SnapSection,
    LandingPageHighlightsSNAP,
    LandingPageSingleListingSNAP,
    LandingPageOrderedListingSNAP
}
