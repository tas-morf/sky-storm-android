package com.accedo.android.skystorm.model.converter;

import com.accedo.android.skystorm.model.bean.client.Session;
import com.accedo.android.skystorm.model.bean.client.SessionError;
import com.accedo.android.skystorm.model.bean.server.ServerUserData;

public class SessionConverter implements Converter<ServerUserData, Session> {

    @Override
    public Session convert(ServerUserData from) {
        if(!"OK".equals(from.getResultMessage())) {
            return  Session.aSession()
                    .isError(true)
                    .sessionId(from.getSkygoSessionId())
                    .sessionError(SessionError.from(Integer.parseInt(from.getResultCode().substring(2))))
                    .remainingAttempts(from.getRemainingAttempt())
                    .build();
        }

        return Session.aSession()
                .customerId(from.getCustomerId())
                .name(from.getFirstName() + " " + from.getLastName())
                .sessionId(from.getSkygoSessionId())
                .build();
    }
}
