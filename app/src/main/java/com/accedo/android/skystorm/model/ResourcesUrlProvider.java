package com.accedo.android.skystorm.model;

import android.content.res.Resources;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.model.bean.client.LoginCredentials;
import com.accedo.android.skystorm.model.persistence.SimplePreferences;
import com.accedo.android.skystorm.util.SafeUrlEncoderDecoder;


/**
 * Uses the android resources in order to provide urls
 */
public class ResourcesUrlProvider implements UrlProvider {

    private Resources resources;
    private SimplePreferences prefs;

    public ResourcesUrlProvider(Resources resources, SimplePreferences prefs) {
        this.resources = resources;
        this.prefs = prefs;
    }

    @Override
    public String allChannelsUrl() {
        return resources.getString(R.string.ehub_base_url) + resources.getString(R
                .string.channels_path);
    }

    @Override
    public String loginUrl(LoginCredentials loginCredentials) {
        String path;
        if (loginCredentials.getEmail() != null && !loginCredentials.getEmail().isEmpty()) {
            path = resources.getString(R.string.login_email_path, loginCredentials.getEmail(), loginCredentials.getPassword());
        } else {
            path = resources.getString(R.string.login_customer_id_path, loginCredentials.getCustomerId(),
                    loginCredentials.getPassword());
        }
        return resources.getString(R.string.silk_base_url)
                + path
                + resources.getString(R.string.silk_extra_params)
                + "&deviceId=" + prefs.getString(SimplePreferences.DEVICE_ID)
                + "&friendlyName=" + SafeUrlEncoderDecoder.encodeUtf8(prefs.getString(SimplePreferences.DEVICE_FRIENDLY_NAME));
    }

    @Override
    public String sessionUrl() {
        return resources.getString(R.string.silk_base_url) + resources.getString(R.string.logout_path,
                prefs.getString(SimplePreferences.SESSION_ID)) + resources.getString(R.string.silk_extra_params);
    }

    @Override
    public String userDataUrl() {
        return resources.getString(R.string.silk_base_url) + resources.getString(R.string.user_data_path,
                prefs.getString(SimplePreferences.SESSION_ID)) + resources.getString(R.string.silk_extra_params);
    }

    @Override
    public String sessionKillUrl(String sessionId) {
        return resources.getString(R.string.silk_base_url) + resources.getString(R.string.kill_session_path,
                sessionId) + resources.getString(R.string.silk_extra_params);
    }

    @Override
    public String getAcceptTermsUrl(String sessionId) {
        return resources.getString(R.string.silk_base_url) + resources.getString(R.string.accept_terms_path,
               sessionId) + resources.getString(R.string.silk_extra_params);
    }

    @Override
    public String navigationUrl() {
        return resources.getString(R.string.base_url) + resources.getString(R.string.navigation_path);
    }

    @Override
    public String searchUrl(String searchTerm) {
        return resources.getString(R.string.silk_base_url) + resources.getString(R.string.search_path,
                searchTerm) + resources.getString(R.string.silk_extra_params);
    }

}
