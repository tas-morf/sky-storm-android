package com.accedo.android.skystorm.model.bean.client;

/**
 * Contains the necessary information for the client session
 */
public class Session {

    private String customerId;
    private String sessionId;
    private String name;
    private boolean isError;
    private SessionError sessionError;
    private int remainingAttempts;

    private Session(Builder builder) {
        customerId = builder.customerId;
        sessionId = builder.sessionId;
        name = builder.name;
        isError = builder.isError;
        sessionError = builder.sessionError;
        remainingAttempts = builder.remainingAttempts;
    }

    public static Builder aSession() {
        return new Builder();
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getName() {
        return name;
    }

    public boolean isError() {
        return isError;
    }

    public SessionError getSessionError() {
        return sessionError;
    }

    public int getRemainingAttempts() {
        return remainingAttempts;
    }

    public static final class Builder {
        private String customerId;
        private String sessionId;
        private String name;
        private boolean isError;
        private SessionError sessionError;
        private int remainingAttempts;

        private Builder() {
        }

        public Builder customerId(String customerId) {
            this.customerId = customerId;
            return this;
        }

        public Builder sessionId(String sessionId) {
            this.sessionId = sessionId;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder isError(boolean isError) {
            this.isError = isError;
            return this;
        }

        public Builder sessionError(SessionError sessionError) {
            this.sessionError = sessionError;
            return this;
        }

        public Builder remainingAttempts(int remainingAttempts) {
            this.remainingAttempts = remainingAttempts;
            return this;
        }

        public Session build() {
            return new Session(this);
        }
    }
}
