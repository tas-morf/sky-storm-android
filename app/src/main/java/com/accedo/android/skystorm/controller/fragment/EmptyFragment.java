package com.accedo.android.skystorm.controller.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.accedo.android.skystorm.R;


public class EmptyFragment extends Fragment {

    private static final String PARAM_TITLE = "title";

    public static Fragment newInstance(String title) {
        Fragment result = new EmptyFragment();
        Bundle args = new Bundle();
        args.putString(PARAM_TITLE, title);
        result.setArguments(args);
        return result;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_empty, container, false);
        ((TextView) rootView.findViewById(R.id.fragment_title)).setText(getArguments().getString(PARAM_TITLE));
        return rootView;
    }
}
