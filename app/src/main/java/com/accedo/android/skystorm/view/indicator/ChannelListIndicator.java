package com.accedo.android.skystorm.view.indicator;

import android.view.View;

import com.accedo.android.skystorm.controller.listener.OnChannelClickListener;
import com.accedo.android.skystorm.model.bean.client.Channel;

import java.util.List;

/**
 * Handles the actual displaying of a list opf movies
 */
public interface ChannelListIndicator {

    void initialize(View rootView, OnChannelClickListener onChannelClickListener);
    void showData(List<Channel> movies);
    void showError();
}
