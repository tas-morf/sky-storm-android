package com.accedo.android.skystorm.module.model;

import com.accedo.android.skystorm.model.bean.client.Channel;
import com.accedo.android.skystorm.model.bean.client.KeepAlive;
import com.accedo.android.skystorm.model.bean.client.NavigationItem;
import com.accedo.android.skystorm.model.bean.client.SearchResult;
import com.accedo.android.skystorm.model.bean.client.Session;
import com.accedo.android.skystorm.model.bean.client.UserData;
import com.accedo.android.skystorm.model.bean.server.ServerChannelsResponse;
import com.accedo.android.skystorm.model.bean.server.ServerKeepAliveResponse;
import com.accedo.android.skystorm.model.bean.server.ServerNavigationResponse;
import com.accedo.android.skystorm.model.bean.server.ServerSearchResponse;
import com.accedo.android.skystorm.model.bean.server.ServerSimpleResponse;
import com.accedo.android.skystorm.model.bean.server.ServerUserData;
import com.accedo.android.skystorm.model.converter.ChannelsConverter;
import com.accedo.android.skystorm.model.converter.Converter;
import com.accedo.android.skystorm.model.converter.KeepAliveConverter;
import com.accedo.android.skystorm.model.converter.LogoutConverter;
import com.accedo.android.skystorm.model.converter.NavigationConverter;
import com.accedo.android.skystorm.model.converter.SearchConverter;
import com.accedo.android.skystorm.model.converter.SessionConverter;
import com.accedo.android.skystorm.model.converter.UserDataConverter;

import java.util.List;

import static com.accedo.android.skystorm.module.ResourcesModule.resources;

public class ConverterModule {
    public static Converter<ServerChannelsResponse, List<Channel>> channelConverter() {
        return new ChannelsConverter(resources());
    }

    public static Converter<ServerUserData, Session> sessionConverter() {
        return new SessionConverter();
    }

    public static Converter<ServerSimpleResponse, Boolean> simpleConverter() {
        return new LogoutConverter();
    }

    public static Converter<ServerUserData, UserData> userDataConverter() {
        return new UserDataConverter();
    }

    public static Converter<ServerKeepAliveResponse, KeepAlive> keepAliveConverter() {
        return new KeepAliveConverter();
    }

    public static Converter<ServerNavigationResponse, List<NavigationItem>> navigationConverter() {
        return new NavigationConverter(resources());
    }

    public static Converter<ServerSearchResponse, List<SearchResult>> searchConverter() {
        return new SearchConverter(resources());
    }
}
