package com.accedo.android.skystorm.controller.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.controller.listener.OnChannelClickListener;
import com.accedo.android.skystorm.model.bean.client.Channel;
import com.accedo.android.skystorm.view.indicator.ChannelListIndicator;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.accedo.android.skystorm.controller.activity.SynopsisDialogActivity;
import com.accedo.android.skystorm.model.UrlProvider;
import com.accedo.android.skystorm.model.request.RequestFactory;

import java.util.List;

import static com.accedo.android.skystorm.module.model.RequestFactoryModule.allChannelsRequestFactory;
import static com.accedo.android.skystorm.module.model.RequestQueueModule.requestQueue;
import static com.accedo.android.skystorm.module.model.UrlProviderModule.urlProvider;
import static com.accedo.android.skystorm.module.view.IndicatorModule.channelListIndicator;

/**
 * Downloads and then uses an indicator to display a list of movies
 */
public class ChannelListFragment extends BaseFragment implements Response.Listener<List<Channel>>,
        Response.ErrorListener,
        OnChannelClickListener {

    private RequestQueue requestQueue;
    private RequestFactory<List<Channel>> channelsRequestFactory;
    private ChannelListIndicator indicator;
    private UrlProvider urlProvider;

    public static Fragment newInstance() {
        Fragment result = new ChannelListFragment();
        Bundle args = new Bundle();
        result.setArguments(args);
        return result;
    }

    public ChannelListFragment() {
        this(requestQueue(), allChannelsRequestFactory(), channelListIndicator(),
                urlProvider());
    }

    public ChannelListFragment(RequestQueue requestQueue, RequestFactory<List<Channel>> channelsRequestFactory,
                               ChannelListIndicator indicator, UrlProvider urlProvider) {
        this.requestQueue = requestQueue;
        this.channelsRequestFactory = channelsRequestFactory;
        this.indicator = indicator;
        this.urlProvider = urlProvider;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_channel_list, container, false);
        indicator.initialize(view, this);
        requestQueue.add(channelsRequestFactory.createRequest(urlProvider.allChannelsUrl(), this, this));
        return view;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        indicator.showError();
    }

    @Override
    public void onResponse(List<Channel> response) {
        indicator.showData(response);
    }

    @Override
    public void onChannelClicked(Channel channel) {
        startActivity(SynopsisDialogActivity.getSynopsisDialogIntent(getActivityOveride(), channel.getTitle(), channel.getMediaUrl()));
    }
}
