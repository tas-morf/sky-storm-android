package com.accedo.android.skystorm.module.model;

import android.preference.PreferenceManager;

import com.accedo.android.skystorm.model.persistence.SimpleAndroidPreferences;
import com.accedo.android.skystorm.model.persistence.SimplePreferences;

import static com.accedo.android.skystorm.module.ApplicationModule.applicationContext;

public class SimplePreferencesModule {
    public static SimplePreferences defaultSimplePreferences() {
        return new SimpleAndroidPreferences(PreferenceManager.getDefaultSharedPreferences(applicationContext()));
    }
}
