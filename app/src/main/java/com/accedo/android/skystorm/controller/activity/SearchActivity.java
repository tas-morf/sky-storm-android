package com.accedo.android.skystorm.controller.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.controller.listener.OnSearchListener;
import com.accedo.android.skystorm.model.UrlProvider;
import com.accedo.android.skystorm.model.bean.client.SearchResult;
import com.accedo.android.skystorm.model.request.RequestFactory;
import com.accedo.android.skystorm.view.indicator.SearchIndicator;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.List;

import static com.accedo.android.skystorm.module.model.RequestFactoryModule.searchRequestFactory;
import static com.accedo.android.skystorm.module.model.RequestQueueModule.requestQueue;
import static com.accedo.android.skystorm.module.model.UrlProviderModule.urlProvider;
import static com.accedo.android.skystorm.module.view.IndicatorModule.searchIndicator;

/**
 * Handles search requests
 */
public class SearchActivity extends Activity implements Response.ErrorListener, Response.Listener<List<SearchResult>>,OnSearchListener {

    private RequestQueue requestQueue;
    private UrlProvider urlProvider;
    private RequestFactory<List<SearchResult>> searchRequestFactory;
    private SearchIndicator searchIndicator;

    public static Intent getSearchIntent(Context context) {
        return new Intent(context, SearchActivity.class);
    }

    public SearchActivity() {
        this(requestQueue(), urlProvider(),
                searchRequestFactory(), searchIndicator());
    }

    public SearchActivity(RequestQueue requestQueue, UrlProvider urlProvider, RequestFactory<List<SearchResult>>
            searchRequestFactory, SearchIndicator searchIndicator) {
        this.requestQueue = requestQueue;
        this.urlProvider = urlProvider;
        this.searchRequestFactory = searchRequestFactory;
        this.searchIndicator = searchIndicator;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_keepalive);
        searchIndicator.initialize(findViewById(R.id.search_content), this);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        searchIndicator.modifySearchItem(menu.findItem(R.id.menu_search));
        return true;
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_keepalive, R.anim.fade_out);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        searchIndicator.showError();
    }

    @Override
    public void onResponse(List<SearchResult> response) {
        searchIndicator.showData(response);
    }

    @Override
    public void onSearch(String searchTerm) {
        requestQueue.add(searchRequestFactory.createRequest(urlProvider.searchUrl(searchTerm), this, this));
    }
}
