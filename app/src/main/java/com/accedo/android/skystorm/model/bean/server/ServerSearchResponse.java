package com.accedo.android.skystorm.model.bean.server;

import java.util.List;

public class ServerSearchResponse {

    private int totElementResult;
    private boolean hasNext;
    private String resultCode;
    private String resultMessage;
    private List<ServerAsset> assetListResult;

    public int getTotElementResult() {
        return totElementResult;
    }

    public boolean isHasNext() {
        return hasNext;
    }

    public String getResultCode() {
        return resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public List<ServerAsset> getAssetListResult() {
        return assetListResult;
    }
}
