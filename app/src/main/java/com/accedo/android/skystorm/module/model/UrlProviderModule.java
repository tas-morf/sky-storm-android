package com.accedo.android.skystorm.module.model;

import com.accedo.android.skystorm.model.ResourcesUrlProvider;
import com.accedo.android.skystorm.model.UrlProvider;

import static com.accedo.android.skystorm.module.ResourcesModule.resources;
import static com.accedo.android.skystorm.module.model.SimplePreferencesModule.defaultSimplePreferences;

public class UrlProviderModule {

    public static UrlProvider urlProvider() {
        return new ResourcesUrlProvider(resources(), defaultSimplePreferences());
    }
}
