package com.accedo.android.skystorm.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.model.bean.client.Channel;
import com.accedo.android.skystorm.view.widget.MovieRowView;

import java.util.List;

/**
 * Displays movies
 */
public class MovieAdapter extends BaseAdapter {
    private final Context context;
    private final List<Channel> movies;

    public MovieAdapter(Context context, List<Channel> movies) {
        this.context = context;
        this.movies = movies;
    }

    @Override
    public int getCount() {
        return movies.size();
    }

    @Override
    public Object getItem(int i) {
        return movies.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.view_movie_row, viewGroup, false);
        }
        ((MovieRowView)convertView).setData(movies.get(position));
        return convertView;
    }
}
