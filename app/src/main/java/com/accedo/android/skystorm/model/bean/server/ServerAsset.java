package com.accedo.android.skystorm.model.bean.server;

import com.accedo.android.skystorm.model.YesNoBooleanDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class ServerAsset {
    private long id;
    private ServerAssetContentType contentType;
    private String title;
    private String subtitle;
    private String description;
    private String category;
    private String nation;
    private int year;
    private String director;
    private String picture;
    private String catalog;
    private ServerImageAsset dvdCover;
    private ServerParentalRating parentalRating;
    private String url;
    @JsonDeserialize(using = YesNoBooleanDeserializer.class)
    private boolean hd;

    public long getId() {
        return id;
    }

    public ServerAssetContentType getContentType() {
        return contentType;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getDescription() {
        return description;
    }

    public String getCategory() {
        return category;
    }

    public String getNation() {
        return nation;
    }

    public int getYear() {
        return year;
    }

    public String getDirector() {
        return director;
    }

    public String getPicture() {
        return picture;
    }

    public String getCatalog() {
        return catalog;
    }

    public ServerImageAsset getDvdCover() {
        return dvdCover;
    }

    public ServerParentalRating getParentalRating() {
        return parentalRating;
    }

    public String getUrl() {
        return url;
    }

    public boolean isHd() {
        return hd;
    }
}
