package com.accedo.android.skystorm.module.view;

import android.app.ActivityManager;
import android.content.Context;

import com.android.volley.toolbox.ImageLoader.ImageCache;
import com.accedo.android.skystorm.view.LruImageCache;

import static com.accedo.android.skystorm.module.ApplicationModule.applicationContext;

public class ImageCacheModule {

	private static ImageCache imageCache = new LruImageCache(getCacheSize());

	public static ImageCache imageCache() {
		return imageCache;
	}

	private static int getCacheSize() {
		int memClass = ((ActivityManager) applicationContext().getSystemService(Context.ACTIVITY_SERVICE))
				.getMemoryClass();
        //use an eighth of the available memory
		return memClass * 1024 * 1024 / 8;
	}

}
