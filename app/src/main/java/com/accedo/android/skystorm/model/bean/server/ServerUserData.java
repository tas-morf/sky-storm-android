package com.accedo.android.skystorm.model.bean.server;

import com.accedo.android.skystorm.model.YesNoBooleanDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

/**
 * Contains the information for the user data that comes back from the server
 */
public class ServerUserData {

    private String skygoSessionId;
    private String country;
    private String email;
    private String customerId;
    private String customerCode;
    private String presentation;
    private String firstName;
    private String lastName;
    @JsonDeserialize(using = YesNoBooleanDeserializer.class)
    private boolean tcFlag;
    @JsonDeserialize(using = YesNoBooleanDeserializer.class)
    private boolean privacyFlag;
    private String ccExpire;
    private int dunning;
    private boolean tvod;
    private List<String> entitlements;
    private List<String> dayPasses;
    private List<ServerSubscription> subscriptions;
    private List<ServerAddon> addOns;
    private String resultCode;
    private String resultMessage;
    private int remainingAttempt;

    public ServerUserData() {
        //required by jackson
    }

    private ServerUserData(Builder builder) {
        skygoSessionId = builder.skygoSessionId;
        country = builder.country;
        email = builder.email;
        customerId = builder.customerId;
        customerCode = builder.customerCode;
        presentation = builder.presentation;
        firstName = builder.firstName;
        lastName = builder.lastName;
        tcFlag = builder.tcFlag;
        privacyFlag = builder.privacyFlag;
        ccExpire = builder.ccExpire;
        dunning = builder.dunning;
        tvod = builder.tvod;
        entitlements = builder.entitlements;
        dayPasses = builder.dayPasses;
        subscriptions = builder.subscriptions;
        addOns = builder.addOns;
        resultCode = builder.resultCode;
        resultMessage = builder.resultMessage;
        remainingAttempt = builder.remainingAttempt;
    }

    /**
     * Do not use in code
     */
    public static Builder newBuilder() {
        return new Builder();
    }

    public String getSkygoSessionId() {
        return skygoSessionId;
    }

    public String getCountry() {
        return country;
    }

    public String getEmail() {
        return email;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getPresentation() {
        return presentation;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public boolean isTcFlag() {
        return tcFlag;
    }

    public boolean isPrivacyFlag() {
        return privacyFlag;
    }

    public String getCcExpire() {
        return ccExpire;
    }

    public int getDunning() {
        return dunning;
    }

    public boolean isTvod() {
        return tvod;
    }

    public List<String> getEntitlements() {
        return entitlements;
    }

    public List<String> getDayPasses() {
        return dayPasses;
    }

    public List<ServerSubscription> getSubscriptions() {
        return subscriptions;
    }

    public String getResultCode() {
        return resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public int getRemainingAttempt() {
        return remainingAttempt;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public List<ServerAddon> getAddOns() {
        return addOns;
    }

    public static final class Builder {
        private String skygoSessionId;
        private String country;
        private String email;
        private String customerId;
        private String customerCode;
        private String presentation;
        private String firstName;
        private String lastName;
        private boolean tcFlag;
        private boolean privacyFlag;
        private String ccExpire;
        private int dunning;
        private boolean tvod;
        private List<String> entitlements;
        private List<String> dayPasses;
        private List<ServerSubscription> subscriptions;
        private List<ServerAddon> addOns;
        private String resultCode;
        private String resultMessage;
        private int remainingAttempt;

        private Builder() {
        }

        public Builder skygoSessionId(String skygoSessionId) {
            this.skygoSessionId = skygoSessionId;
            return this;
        }

        public Builder country(String country) {
            this.country = country;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder customerId(String customerId) {
            this.customerId = customerId;
            return this;
        }

        public Builder customerCode(String customerCode) {
            this.customerCode = customerCode;
            return this;
        }

        public Builder presentation(String presentation) {
            this.presentation = presentation;
            return this;
        }

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder tcFlag(boolean tcFlag) {
            this.tcFlag = tcFlag;
            return this;
        }

        public Builder privacyFlag(boolean privacyFlag) {
            this.privacyFlag = privacyFlag;
            return this;
        }

        public Builder ccExpire(String ccExpire) {
            this.ccExpire = ccExpire;
            return this;
        }

        public Builder dunning(int dunning) {
            this.dunning = dunning;
            return this;
        }

        public Builder tvod(boolean tvod) {
            this.tvod = tvod;
            return this;
        }

        public Builder entitlements(List<String> entitlements) {
            this.entitlements = entitlements;
            return this;
        }

        public Builder dayPasses(List<String> dayPasses) {
            this.dayPasses = dayPasses;
            return this;
        }

        public Builder subscriptions(List<ServerSubscription> subscriptions) {
            this.subscriptions = subscriptions;
            return this;
        }

        public Builder addOns(List<ServerAddon> addOns) {
            this.addOns = addOns;
            return this;
        }

        public Builder resultCode(String resultCode) {
            this.resultCode = resultCode;
            return this;
        }

        public Builder resultMessage(String resultMessage) {
            this.resultMessage = resultMessage;
            return this;
        }

        public Builder remainingAttempt(int remainingAttempt) {
            this.remainingAttempt = remainingAttempt;
            return this;
        }

        public ServerUserData build() {
            return new ServerUserData(this);
        }
    }
}
