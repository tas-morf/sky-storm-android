package com.accedo.android.skystorm.view.indicator;

import android.view.View;

import com.accedo.android.skystorm.controller.listener.OnLoginEventListener;
import com.accedo.android.skystorm.model.bean.client.LoginCredentials;
import com.accedo.android.skystorm.model.bean.client.Session;
import com.accedo.android.skystorm.model.bean.client.SessionError;

/**
 * Handles UI for the login functionality
 */
public interface LoginIndicator {
    void initialize(View rootView, OnLoginEventListener listener);

    void showNetworkError();

    void showGreeting(String name);

    void showServerError(SessionError sessionError, int remainingAttempts);

    LoginCredentials getLoginCredentials();

    void sessionKillFailed();
}
