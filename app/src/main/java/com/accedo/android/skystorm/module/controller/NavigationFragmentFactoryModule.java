package com.accedo.android.skystorm.module.controller;

import com.accedo.android.skystorm.controller.DefaultNavigationFragmentFactory;
import com.accedo.android.skystorm.controller.NavigationFragmentFactory;

public class NavigationFragmentFactoryModule {
    public static NavigationFragmentFactory navigationFragmentFactory() {
        return new DefaultNavigationFragmentFactory();
    }
}
