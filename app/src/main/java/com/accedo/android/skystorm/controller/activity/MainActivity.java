package com.accedo.android.skystorm.controller.activity;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.controller.NavigationFragmentFactory;
import com.accedo.android.skystorm.controller.fragment.SideMenuFragment;
import com.accedo.android.skystorm.controller.listener.SideMenuEventListener;
import com.accedo.android.skystorm.model.bean.client.NavigationItem;

import static com.accedo.android.skystorm.module.controller.NavigationFragmentFactoryModule.navigationFragmentFactory;


public class MainActivity extends Activity implements SideMenuEventListener, View.OnClickListener {


    private static final int LOGIN_REQUEST_CODE = 100;
    private static final int LOGOUT_REQUEST_CODE = 101;

    private NavigationFragmentFactory navigationFragmentFactory;

    private ActionBarDrawerToggle drawerToggle;
    private DrawerLayout drawerLayout;
    private LinearLayout submenuTabContainer;
    private View contentPlaceHolder;

    public MainActivity() {
        this(navigationFragmentFactory());
    }

    public MainActivity(NavigationFragmentFactory navigationFragmentFactory) {
        this.navigationFragmentFactory = navigationFragmentFactory;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer, R.string.drawer_open,
                R.string.drawer_close);
        drawerLayout.setDrawerListener(drawerToggle);
        submenuTabContainer = (LinearLayout) findViewById(R.id.submenu_tab_container);
        contentPlaceHolder = findViewById(R.id.content_placeholder);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        getFragmentManager().beginTransaction().replace(R.id.menu_placeholder, SideMenuFragment.newInstance()).commit();
    }

    public static Intent getMainActivityIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        if (item.getItemId() == R.id.menu_search) {
            startActivity(SearchActivity.getSearchIntent(this));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LOGIN_REQUEST_CODE || requestCode == LOGOUT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                drawerLayout.closeDrawers();
                handleUserStatusChanged();
            }
        }
    }

    private void handleUserStatusChanged() {
        ((SideMenuFragment) getFragmentManager().findFragmentById(R.id.menu_placeholder)).refreshData();
        Fragment contentFragment = getFragmentManager().findFragmentById(R.id.content_placeholder);
        if (contentFragment != null) {
            getFragmentManager().beginTransaction().remove(contentFragment).commit();
        }
    }

    @Override
    public void onNavigationItemSelected(NavigationItem navItem) {
        drawerLayout.closeDrawers();
        populateSubMenuTabs(navItem);
        getFragmentManager().beginTransaction().replace(R.id.content_placeholder,
                navigationFragmentFactory.fragmentFor(navItem)).commitAllowingStateLoss();
    }

    private void populateSubMenuTabs(NavigationItem navItem) {
        submenuTabContainer.removeAllViews();
        if (navItem.getSubNavigationItems().isEmpty()) {
            submenuTabContainer.setVisibility(View.GONE);
            ((ViewGroup.MarginLayoutParams)contentPlaceHolder.getLayoutParams()).topMargin = 0;
        } else {
            ((ViewGroup.MarginLayoutParams)contentPlaceHolder.getLayoutParams()).topMargin = getResources()
                    .getDimensionPixelSize(R.dimen.submenu_tab_height);
            submenuTabContainer.setVisibility(View.VISIBLE);
            for (NavigationItem subItem : navItem.getSubNavigationItems()) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams
                        .WRAP_CONTENT, getResources()
                        .getDimensionPixelSize(R.dimen.submenu_tab_height));
                Button tab = new Button(this);
                tab.setText(subItem.getTitle());
                tab.setTag(subItem);
                tab.setOnClickListener(this);
                submenuTabContainer.addView(tab, params);
            }
        }
    }

    @Override
    public void onSettingsClicked() {
        drawerLayout.closeDrawers();
    }

    @Override
    public void onLoginClicked() {
        startActivityForResult(LoginActivity.getLoginActivityIntent(this), LOGIN_REQUEST_CODE);
    }

    @Override
    public void onLogoutClicked() {
        startActivityForResult(LogoutActivity.getLogoutIntent(this), LOGOUT_REQUEST_CODE);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawers();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View view) {
        getFragmentManager().beginTransaction().replace(R.id.content_placeholder,
                navigationFragmentFactory.fragmentFor((NavigationItem) view.getTag())).commitAllowingStateLoss();
    }
}
