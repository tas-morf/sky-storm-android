package com.accedo.android.skystorm.model.converter;

/**
 * Converts for something into something else (used strictly for beans, as it is too generic)
 */
public interface Converter<F, T> {

    T convert(F from);
}
