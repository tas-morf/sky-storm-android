package com.accedo.android.skystorm.model.bean.server;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * A single navigation node as represented in the server
 */
public class ServerNavigationNode {
    private ServerNodeType typeNode;
    private int id;
    private ServerStructureType structureType;
    private String label;
    private String logo;
    private String path;
    private boolean hide;
    private int featureMunVersion;
    private int featureMaxVersion;
    @JsonProperty("id_backgroundselector")
    private int idBackgroundSelector;
    private boolean displayFirstChild;
    private boolean expandable;
    private List<ServerNavigationNode> node;

    public ServerNavigationNode() {
        //required by Jackson
    }

    private ServerNavigationNode(Builder builder) {
        typeNode = builder.typeNode;
        id = builder.id;
        structureType = builder.structureType;
        label = builder.label;
        logo = builder.logo;
        path = builder.path;
        hide = builder.hide;
        featureMunVersion = builder.featureMunVersion;
        featureMaxVersion = builder.featureMaxVersion;
        idBackgroundSelector = builder.idBackgroundSelector;
        displayFirstChild = builder.displayFirstChild;
        expandable = builder.expandable;
        node = builder.node;
    }

    /**
     * Do not use outside tests
     */
    public static Builder newBuilder() {
        return new Builder();
    }

    public ServerNodeType getTypeNode() {
        return typeNode;
    }

    public int getId() {
        return id;
    }

    public ServerStructureType getStructureType() {
        return structureType;
    }

    public String getLabel() {
        return label;
    }

    public String getLogo() {
        return logo;
    }

    public String getPath() {
        return path;
    }

    public boolean isHide() {
        return hide;
    }

    public int getFeatureMunVersion() {
        return featureMunVersion;
    }

    public int getFeatureMaxVersion() {
        return featureMaxVersion;
    }

    public int getIdBackgroundSelector() {
        return idBackgroundSelector;
    }

    public boolean isDisplayFirstChild() {
        return displayFirstChild;
    }

    public boolean isExpandable() {
        return expandable;
    }

    public List<ServerNavigationNode> getNode() {
        return node;
    }

    public static final class Builder {
        private ServerNodeType typeNode;
        private int id;
        private ServerStructureType structureType;
        private String label;
        private String logo;
        private String path;
        private boolean hide;
        private int featureMunVersion;
        private int featureMaxVersion;
        private int idBackgroundSelector;
        private boolean displayFirstChild;
        private boolean expandable;
        private List<ServerNavigationNode> node;

        private Builder() {
        }

        public Builder typeNode(ServerNodeType typeNode) {
            this.typeNode = typeNode;
            return this;
        }

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder structureType(ServerStructureType structureType) {
            this.structureType = structureType;
            return this;
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Builder logo(String logo) {
            this.logo = logo;
            return this;
        }

        public Builder path(String path) {
            this.path = path;
            return this;
        }

        public Builder hide(boolean hide) {
            this.hide = hide;
            return this;
        }

        public Builder featureMunVersion(int featureMunVersion) {
            this.featureMunVersion = featureMunVersion;
            return this;
        }

        public Builder featureMaxVersion(int featureMaxVersion) {
            this.featureMaxVersion = featureMaxVersion;
            return this;
        }

        public Builder idBackgroundSelector(int idBackgroundSelector) {
            this.idBackgroundSelector = idBackgroundSelector;
            return this;
        }

        public Builder displayFirstChild(boolean displayFirstChild) {
            this.displayFirstChild = displayFirstChild;
            return this;
        }

        public Builder expandable(boolean expandable) {
            this.expandable = expandable;
            return this;
        }

        public Builder node(List<ServerNavigationNode> node) {
            this.node = node;
            return this;
        }

        public ServerNavigationNode build() {
            return new ServerNavigationNode(this);
        }
    }
}
