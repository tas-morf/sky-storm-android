package com.accedo.android.skystorm.view.indicator.impl;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.controller.listener.SideMenuEventListener;
import com.accedo.android.skystorm.model.bean.client.NavigationItem;
import com.accedo.android.skystorm.view.Toaster;
import com.accedo.android.skystorm.view.adapter.SideMenuAdapter;
import com.accedo.android.skystorm.view.indicator.SideMenuIndicator;

import java.util.List;

public class SideMenuViewIndicator implements SideMenuIndicator, View.OnClickListener, AdapterView.OnItemClickListener {

    private View progress;
    private ListView expListView;
    private Toaster toaster;
    private SideMenuEventListener sideMenuEventListener;
    private View loginButton;
    private View logoutButton;

    public SideMenuViewIndicator(Toaster toaster) {
        this.toaster = toaster;
    }

    @Override
    public void initialize(View rootView, SideMenuEventListener sideMenuEventListener) {
        this.sideMenuEventListener = sideMenuEventListener;
        progress = rootView.findViewById(R.id.progress);
        expListView  = (ListView)rootView.findViewById(R.id.side_menu_list);
        loginButton = rootView.findViewById(R.id.login_button);
        logoutButton = rootView.findViewById(R.id.logout_button);
        loginButton.setOnClickListener(this);
        logoutButton.setOnClickListener(this);
        rootView.findViewById(R.id.setting_button).setOnClickListener(this);
        expListView.setOnItemClickListener(this);
    }

    @Override
    public void showSideMenu(List<NavigationItem> items, boolean isSignedIn) {
        expListView.setVisibility(View.VISIBLE);
        progress.setVisibility(View.GONE);
        expListView.setAdapter(new SideMenuAdapter(expListView.getContext(), items));
        if(isSignedIn) {
            loginButton.setVisibility(View.GONE);
            logoutButton.setVisibility(View.VISIBLE);
        } else {
            loginButton.setVisibility(View.VISIBLE);
            logoutButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void showError() {
        toaster.showToast(R.string.side_menu_error_toast);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.setting_button:
                sideMenuEventListener.onSettingsClicked();
                break;
            case R.id.login_button:
                sideMenuEventListener.onLoginClicked();
                break;
            case R.id.logout_button:
                sideMenuEventListener.onLogoutClicked();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        NavigationItem navItem = (NavigationItem)adapterView.getAdapter().getItem(i);
        sideMenuEventListener.onNavigationItemSelected(navItem);
    }
}
