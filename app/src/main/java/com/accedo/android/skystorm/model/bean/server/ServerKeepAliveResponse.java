package com.accedo.android.skystorm.model.bean.server;

/**
 * Contains the server response when doing a keep alive request
 */
public class ServerKeepAliveResponse {

    private String resultCode;
    private String resultMessage;
    private boolean reloadUserData;

    public ServerKeepAliveResponse() {
        //required by jackson
    }

    private ServerKeepAliveResponse(Builder builder) {
        resultCode = builder.resultCode;
        resultMessage = builder.resultMessage;
        reloadUserData = builder.reloadUserData;
    }

    /**
     * Do not use in code
     */
    public static Builder newBuilder() {
        return new Builder();
    }

    public String getResultCode() {
        return resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public boolean isReloadUserData() {
        return reloadUserData;
    }


    public static final class Builder {
        private String resultCode;
        private String resultMessage;
        private boolean reloadUserData;

        private Builder() {
        }

        public Builder resultCode(String resultCode) {
            this.resultCode = resultCode;
            return this;
        }

        public Builder resultMessage(String resultMessage) {
            this.resultMessage = resultMessage;
            return this;
        }

        public Builder reloadUserData(boolean reloadUserData) {
            this.reloadUserData = reloadUserData;
            return this;
        }

        public ServerKeepAliveResponse build() {
            return new ServerKeepAliveResponse(this);
        }
    }
}
