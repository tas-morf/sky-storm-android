package com.accedo.android.skystorm.model.bean.server;

public class ServerImageAsset {
    String value;
    String type;

    public String getValue() {
        return value;
    }

    public String getType() {
        return type;
    }
}
