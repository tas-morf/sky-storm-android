package com.accedo.android.skystorm.view.indicator;


import android.view.View;

import com.accedo.android.skystorm.controller.listener.SideMenuEventListener;
import com.accedo.android.skystorm.model.bean.client.NavigationItem;

import java.util.List;

public interface SideMenuIndicator {
    void initialize(View rootView, SideMenuEventListener sideMenuEventListener);

    void showSideMenu(List<NavigationItem> items, boolean isSignedIn);

    void showError();
}
