package com.accedo.android.skystorm.view.indicator;

import android.view.View;

import com.accedo.android.skystorm.controller.listener.OnLogoutEventListener;

public interface LogoutIndicator {
    void initialize(View view, OnLogoutEventListener listener);

    void showError();

    void showSuccessMessage();

}
