package com.accedo.android.skystorm.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.model.bean.client.NavigationItem;

public class SideMenuRowView extends LinearLayout {
    private TextView title;

    public SideMenuRowView(Context context) {
        super(context);
    }

    public SideMenuRowView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SideMenuRowView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        title = (TextView)findViewById(R.id.title);

    }

    public void setData(NavigationItem navigationItem) {
        title.setText(navigationItem.getTitle().toUpperCase());
    }
}
