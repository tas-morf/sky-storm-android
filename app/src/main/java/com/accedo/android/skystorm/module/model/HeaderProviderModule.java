package com.accedo.android.skystorm.module.model;

import com.accedo.android.skystorm.model.header.HeaderProvider;
import com.accedo.android.skystorm.model.header.PreferencesHeaderProvider;

import static com.accedo.android.skystorm.module.model.SimplePreferencesModule.defaultSimplePreferences;

public class HeaderProviderModule {

    public static HeaderProvider headerProvider() {
        return new PreferencesHeaderProvider(defaultSimplePreferences());
    }
}
