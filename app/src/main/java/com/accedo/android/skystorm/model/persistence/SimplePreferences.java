package com.accedo.android.skystorm.model.persistence;

/**
 * A simple interface for the android {@link android.content.SharedPreferences}. We might not want to use all the
 * methods that exist in that class. Additionally we don't want to be writing all that editor boilerplate code. Finally
 * contains all the keys for the preferences we are going to use in this app.
 */
public interface SimplePreferences {
    String SESSION_ID = "sessionId";
    String DEVICE_ID = "deviceId";
    String DEVICE_FRIENDLY_NAME = "friendlyName";

    String getString(String key);

    void saveString(String key, String value);

    void delete(String... keys);

    boolean contains(String key);
}
