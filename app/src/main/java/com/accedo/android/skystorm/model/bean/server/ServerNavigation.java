package com.accedo.android.skystorm.model.bean.server;

import java.util.List;

/**
 * Navigation as represented by the server
 */
public class ServerNavigation {

    private List<ServerNavigationNode> node;

    public ServerNavigation() {
        //required by jackson
    }

    private ServerNavigation(Builder builder) {
        node = builder.node;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public List<ServerNavigationNode> getNode() {
        return node;
    }

    public static final class Builder {
        private List<ServerNavigationNode> node;

        private Builder() {
        }

        public Builder node(List<ServerNavigationNode> node) {
            this.node = node;
            return this;
        }

        public ServerNavigation build() {
            return new ServerNavigation(this);
        }
    }
}
