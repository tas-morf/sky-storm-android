package com.accedo.android.skystorm.module.view;

import com.accedo.android.skystorm.view.indicator.ChannelListIndicator;
import com.accedo.android.skystorm.view.indicator.LoginIndicator;
import com.accedo.android.skystorm.view.indicator.LogoutIndicator;
import com.accedo.android.skystorm.view.indicator.SearchIndicator;
import com.accedo.android.skystorm.view.indicator.SideMenuIndicator;
import com.accedo.android.skystorm.view.indicator.impl.LoginViewIndicator;
import com.accedo.android.skystorm.view.indicator.impl.LogoutViewIndicator;
import com.accedo.android.skystorm.view.indicator.impl.MovieListViewIndicator;
import com.accedo.android.skystorm.view.indicator.impl.SearchViewIndicator;
import com.accedo.android.skystorm.view.indicator.impl.SideMenuViewIndicator;

import static com.accedo.android.skystorm.module.ResourcesModule.resources;
import static com.accedo.android.skystorm.module.view.ToasterModule.toaster;

public class IndicatorModule {
    public static ChannelListIndicator channelListIndicator() {
        return new MovieListViewIndicator(resources(), toaster());
    }

    public static LoginIndicator loginIndicator() {
        return new LoginViewIndicator(toaster(), resources());
    }

    public static LogoutIndicator logoutIndicator() {
        return new LogoutViewIndicator(toaster());
    }

    public static SideMenuIndicator sideMenuIndicator() {
        return new SideMenuViewIndicator(toaster());
    }

    public static SearchIndicator searchIndicator() {
        return new SearchViewIndicator(resources(), toaster());
    }
}
