package com.accedo.android.skystorm.module;

import android.content.Context;

import com.accedo.android.skystorm.SkystormApplication;

public class ApplicationModule {

    private static SkystormApplication skystormApplication;

    public static void setApplication(SkystormApplication skystormApplication) {
        ApplicationModule.skystormApplication = skystormApplication;
    }

    public static Context applicationContext() {
        return skystormApplication;
    }
}
