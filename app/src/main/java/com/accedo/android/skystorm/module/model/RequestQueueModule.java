package com.accedo.android.skystorm.module.model;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import static com.accedo.android.skystorm.module.ApplicationModule.applicationContext;

public class RequestQueueModule {

    private static RequestQueue requestQueue = Volley.newRequestQueue(applicationContext());

    public static RequestQueue requestQueue() {
        return requestQueue;
    }
}
