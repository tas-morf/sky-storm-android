package com.accedo.android.skystorm.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.accedo.android.skystorm.R;

/**
 * This is a self-contained widget, which has all the necessary information in order to display the epg extract
 */
public class EpgExtractView extends LinearLayout implements View.OnClickListener {

    private ListView eventsList;
    private LinearLayout tabsHolder;

    public EpgExtractView(Context context) {
        super(context);
    }

    public EpgExtractView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EpgExtractView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        findViewById(R.id.title).setOnClickListener(this);
        eventsList = (ListView)findViewById(R.id.events_list);
        tabsHolder = (LinearLayout)findViewById(R.id.tabs_holder);
    }

    @Override
    public void onClick(View view) {

    }
}
