package com.accedo.android.skystorm.controller.listener;

/**
 * Gets notified that the status of the user has changed and behaves accordingly
 */
public interface OnUserStatusChangeListener {

    void onUserStatusChange(boolean isLoggedIn);
}
