package com.accedo.android.skystorm.controller.listener;

/**
 * Listens for search events
 */
public interface OnSearchListener {

    void onSearch(String searchTerm);
}
