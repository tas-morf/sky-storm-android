package com.accedo.android.skystorm.module.view;

import com.android.volley.toolbox.ImageLoader;

import static com.accedo.android.skystorm.module.model.RequestQueueModule.requestQueue;
import static com.accedo.android.skystorm.module.view.ImageCacheModule.imageCache;

public class ImageLoaderModule {

    private static ImageLoader imageLoader = new ImageLoader(requestQueue(), imageCache());

    public static ImageLoader imageLoader() {
        return imageLoader;
    }
}
