package com.accedo.android.skystorm.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;

public class YesNoBooleanDeserializer extends JsonDeserializer<Boolean> {
    @Override
    public Boolean deserialize(JsonParser jsonparser,
                               DeserializationContext deserializationcontext) throws IOException {

        String yesOrNo = jsonparser.getText();
        if(yesOrNo != null && (yesOrNo.equals("Y")|| yesOrNo.equals("yes"))) {
            return true;
        }
        return false;
    }

}