package com.accedo.android.skystorm.model.bean.server;

public class ServerSimpleResponse {
    private String resultCode;
    private String resultMessage;

    public ServerSimpleResponse() {
        //required by jackson
    }

    private ServerSimpleResponse(Builder builder) {
        resultCode = builder.resultCode;
        resultMessage = builder.resultMessage;
    }

    /**
     * Do not use in code
     */
    public static Builder newBuilder() {
        return new Builder();
    }

    public String getResultCode() {
        return resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public static final class Builder {
        private String resultCode;
        private String resultMessage;

        private Builder() {
        }

        public Builder resultCode(String resultCode) {
            this.resultCode = resultCode;
            return this;
        }

        public Builder resultMessage(String resultMessage) {
            this.resultMessage = resultMessage;
            return this;
        }

        public ServerSimpleResponse build() {
            return new ServerSimpleResponse(this);
        }
    }
}
