package com.accedo.android.skystorm.model.bean.client;

public enum KeepAliveCommand {

    START,
    STOP,
    CONTINUE
}
