package com.accedo.android.skystorm.controller.listener;

/**
 * Listens for a successful conclusion of the logout process
 */
public interface OnLogoutProcessFinishedListener {
    void onLogoutProcessFinished();
}
