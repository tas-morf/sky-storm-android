package com.accedo.android.skystorm.model.bean.client;

/**
 * Contains the information tht the client cares about when doing a keep alive request
 */
public class KeepAlive {

    private boolean success;

    private boolean reloadUserData;

    private KeepAlive(Builder builder) {
        success = builder.success;
        reloadUserData = builder.reloadUserData;
    }

    public static Builder aKeepAlive() {
        return new Builder();
    }

    public boolean isReloadUserData() {
        return reloadUserData;
    }

    public boolean isSuccess() {
        return success;
    }


    public static final class Builder {
        private boolean success;
        private boolean reloadUserData;

        private Builder() {
        }

        public Builder success(boolean success) {
            this.success = success;
            return this;
        }

        public Builder reloadUserData(boolean reloadUserData) {
            this.reloadUserData = reloadUserData;
            return this;
        }

        public KeepAlive build() {
            return new KeepAlive(this);
        }
    }
}
