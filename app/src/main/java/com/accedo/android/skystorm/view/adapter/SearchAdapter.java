package com.accedo.android.skystorm.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.model.bean.client.SearchResult;
import com.accedo.android.skystorm.view.widget.MovieRowView;
import com.accedo.android.skystorm.view.widget.SearchResultView;

import java.util.Collections;
import java.util.List;

public class SearchAdapter extends BaseAdapter {
    private List<SearchResult> data;
    private Context context;

    public SearchAdapter(Context context) {
        this.context = context;
        data = Collections.EMPTY_LIST;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.view_search_result, viewGroup, false);
        }
        ((SearchResultView)convertView).setData(data.get(position));
        return convertView;
    }

    public void setData(List<SearchResult> response) {
        data = response;
        notifyDataSetChanged();
    }
}
