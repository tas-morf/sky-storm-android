package com.accedo.android.skystorm.controller.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.controller.AppInForegroundChecker;
import com.accedo.android.skystorm.controller.activity.LoginActivity;
import com.accedo.android.skystorm.model.UrlProvider;
import com.accedo.android.skystorm.model.bean.client.KeepAlive;
import com.accedo.android.skystorm.model.bean.client.KeepAliveCommand;
import com.accedo.android.skystorm.model.persistence.SimplePreferences;
import com.accedo.android.skystorm.model.request.RequestFactory;
import com.accedo.android.skystorm.view.Toaster;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.RequestFuture;

import static com.accedo.android.skystorm.module.controller.AppInForegroundCheckerModule.appInForegroundChecker;
import static com.accedo.android.skystorm.module.controller.HandlerModule.mainThreadHandler;
import static com.accedo.android.skystorm.module.model.RequestFactoryModule.keepAliveRequestFactory;
import static com.accedo.android.skystorm.module.model.RequestQueueModule.requestQueue;
import static com.accedo.android.skystorm.module.model.SimplePreferencesModule.defaultSimplePreferences;
import static com.accedo.android.skystorm.module.model.UrlProviderModule.urlProvider;
import static com.accedo.android.skystorm.module.view.ToasterModule.toaster;

/**
 * Sends KeepAliveRequests every 15 minutes.
 * TODO: A decision is needed here. We ether have a service running every 15 minutes even if the app is closed,
 * and we refresh the token, or we don't do it and leave the user to login again every time they open the app.
 * Or, even save their credentials and do it automatically for them (not secure).
 */
public class KeepAliveIntentService extends IntentService {

    //Do once every 15 minutes
    private static final long KEEP_ALIVE_DELAY = 15 * 60 * 1000;
    private static final String PARAM_COMMAND = "command";

    private final RequestQueue requestQueue;
    private final RequestFactory<KeepAlive> keepAliveRequestFactory;
    private final UrlProvider urlProvider;
    private final Handler handler;
    private final AppInForegroundChecker appInForegroundChecker;
    private final SimplePreferences prefs;
    private final Toaster toaster;

    public static Intent getKeepAliveIntent(Context context, KeepAliveCommand command) {
        Intent intent = new Intent(context, KeepAliveIntentService.class);
        intent.putExtra(PARAM_COMMAND, command);
        return intent;
    }

    public KeepAliveIntentService() {
        super(KeepAliveIntentService.class.getSimpleName());
        requestQueue = requestQueue();
        keepAliveRequestFactory = keepAliveRequestFactory();
        urlProvider = urlProvider();
        handler = mainThreadHandler();
        appInForegroundChecker = appInForegroundChecker();
        prefs = defaultSimplePreferences();
        toaster = toaster();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if(intent.getSerializableExtra(PARAM_COMMAND) == KeepAliveCommand.STOP) {
            handler.removeCallbacks(continueServiceRunnable);
            return;
        }
        RequestFuture<KeepAlive> requestFuture = RequestFuture.newFuture();
        requestQueue.add(keepAliveRequestFactory.createRequest(Request.Method.PUT, urlProvider.sessionUrl(),
                requestFuture, requestFuture));

        KeepAlive result = KeepAlive.aKeepAlive().build();
        try {
            result = requestFuture.get();
        } catch (Exception e) {

        }
        if(result.isSuccess()) {
            handler.postDelayed(continueServiceRunnable, KEEP_ALIVE_DELAY);
            if(result.isReloadUserData()) {
                //TODO: do something here in order to reload the user's data, whatever that means
            }
        } else {
            onKeepAliveFailed();
        }
    }

    private void onKeepAliveFailed() {
            //Clear the session id. That way it will be easier to figure out what went wrong.
            prefs.delete(SimplePreferences.SESSION_ID);
        if(appInForegroundChecker.isAppVisible()) {
            //if the app is visible we should display the login screen
            Intent loginIntent = LoginActivity.getLoginActivityIntent(this);
            loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(loginIntent);
            toaster.showToast(R.string.keep_alive_failed);
        }
    }

    private final Runnable continueServiceRunnable = new Runnable() {
        @Override
        public void run() {
            startService(getKeepAliveIntent(KeepAliveIntentService.this, KeepAliveCommand.CONTINUE));
        }
    };
}
