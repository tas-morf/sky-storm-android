package com.accedo.android.skystorm.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.accedo.android.skystorm.R;
import com.accedo.android.skystorm.model.bean.client.Channel;
import com.android.volley.toolbox.ImageLoader;

import static com.accedo.android.skystorm.module.view.ImageLoaderModule.imageLoader;


/**
 * Represents a single movie
 */
public class MovieRowView extends LinearLayout{
    private TextView title;
    private TextView id;
    private TextView year;
    private ImageView thumbnail;
    private ImageLoader imageLoader;

    public MovieRowView(Context context) {
        super(context);
        postConstruct(imageLoader());
    }

    public MovieRowView(Context context, AttributeSet attrs) {
        super(context, attrs);
        postConstruct(imageLoader());
    }

    public MovieRowView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        postConstruct(imageLoader());
    }

    private void postConstruct(ImageLoader imageLoader) {
        this.imageLoader = imageLoader;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        title = (TextView)findViewById(R.id.title);
        id = (TextView)findViewById(R.id.id);
        year = (TextView)findViewById(R.id.year);
        thumbnail = (ImageView)findViewById(R.id.thumbnail);
    }

    public void setData(Channel channel) {
        title.setText(channel.getTitle());
        id.setText(channel.getId());
        year.setText(String.valueOf(channel.getNum()));
        ImageLoader.ImageContainer imageContainer = (ImageLoader.ImageContainer) thumbnail.getTag();
        if(imageContainer == null || !imageContainer.getRequestUrl().equals(channel.getImageUrl())) {
            thumbnail.setTag(imageLoader.get(channel.getImageUrl(), ImageLoader.getImageListener(thumbnail,
                    R.drawable.ic_launcher, R.drawable.ic_launcher)));
        }
    }
}
