package com.accedo.android.skystorm.module.controller;

import android.os.Handler;

import static com.accedo.android.skystorm.module.ApplicationModule.applicationContext;

public class HandlerModule {
    public static Handler mainThreadHandler() {
        return new Handler(applicationContext().getMainLooper());
    }
}
